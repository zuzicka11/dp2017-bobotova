import SimpleITK as sitk
import vtk
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

# create volume for visualization
class Volume():
    def __init__(self, volume, segmentation):
        self.img = sitk.GetImageFromArray(volume.data)  # SimpleITK object
        self.img.SetOrigin([0.0, 0.0, 0.0])

        mask = sitk.GetImageFromArray(segmentation.data)  # SimpleITK object
        mask_inv = sitk.InvertIntensity(mask)

        self.tumor = sitk.Mask(self.img, mask)
        self.brain = sitk.Mask(self.img, mask_inv)

    def createTumorActor(self):
        # create tumor actor
        data = sitk.GetArrayFromImage(self.tumor).astype('float')  # numpy array
        data *= 255 / data.max()
        tf = [[0, 0, 0, 0, 0], [1, 0, 0, 0, 0], [255, 1, 1, 1, 1]]
        tumor_actor = self.volumeRender(data, tf, self.img.GetSpacing())

        self.tumor_hist, edges = np.histogram(data, bins=11)

        print(type(self.tumor_hist))

        return tumor_actor

    def getTumorHist(self):
        return self.tumor_hist.tolist()

    def createBrainActor(self):
        # create brain actor
        data = sitk.GetArrayFromImage(self.brain).astype('float')  # numpy array
        data *= 255 / data.max()
        tf = [[0, 0, 0, 0, 0], [1, 0, 0, 0, 0], [255, 1, 1, 1, 0.07]]
        brain_actor = self.volumeRender(data, tf, self.img.GetSpacing())

        self.brain_hist, edges = np.histogram(data, bins=11)

        return brain_actor

    def getBrainHist(self):
        return self.brain_hist.tolist()

    # set opacity of the volume
    def setVolumeOpacity(self, opacity, window):
        tf = [[0, 0], [1, 0], [255, opacity]]
        opacity_tf = vtk.vtkPiecewiseFunction()

        for p in tf:
            opacity_tf.AddPoint(p[0], p[1])

        self.volProperty.SetScalarOpacity(opacity_tf)

        window.Render()

    # highlight volume in the visualization
    def highlightVolume(self, window, highlightVal):
        tf = [[0, 0, 0, 0], [1, 0, 0, 0], [255, highlightVal, highlightVal, highlightVal]]
        color_tf = vtk.vtkColorTransferFunction()

        for p in tf:
            color_tf.AddRGBPoint(p[0], p[1], p[2], p[3])

        self.volProperty.SetColor(color_tf)

        window.Render()

    # recolor volume to the red in the visualization
    def recolorVolume(self, window):
        tf = [[0, 0, 0, 0], [1, 0, 0, 0], [255, 255, 0, 0]]
        color_tf = vtk.vtkColorTransferFunction()

        for p in tf:
            color_tf.AddRGBPoint(p[0], p[1], p[2], p[3])

        self.volProperty.SetColor(color_tf)

        window.Render()

    # convert numpy array to VTK object
    def numpy2VTK(self, img, spacing=[1.0, 1.0, 1.0]):
        # evolved from code from Stou S.,
        # on http://www.siafoo.net/snippet/314
        importer = vtk.vtkImageImport()

        img_data = img.astype('uint8')
        img_string = img_data.tostring()  # type short
        dim = img.shape

        importer.CopyImportVoidPointer(img_string, len(img_string))
        importer.SetDataScalarType(vtk.VTK_UNSIGNED_CHAR)
        importer.SetNumberOfScalarComponents(1)

        extent = importer.GetDataExtent()
        importer.SetDataExtent(extent[0], extent[0] + dim[2] - 1,
                               extent[2], extent[2] + dim[1] - 1,
                               extent[4], extent[4] + dim[0] - 1)
        importer.SetWholeExtent(extent[0], extent[0] + dim[2] - 1,
                                extent[2], extent[2] + dim[1] - 1,
                                extent[4], extent[4] + dim[0] - 1)

        importer.SetDataSpacing(spacing[0], spacing[1], spacing[2])
        importer.SetDataOrigin(0, 0, 0)

        return importer

    # render volume
    def volumeRender(self, img, tf=[], spacing=[1.0, 1.0, 1.0]):
        importer = self.numpy2VTK(img, spacing)

        # transfer Functions
        opacity_tf = vtk.vtkPiecewiseFunction()
        color_tf = vtk.vtkColorTransferFunction()

        if len(tf) == 0:
            tf.append([img.min(), 0, 0, 0, 0])
            tf.append([img.max(), 1, 1, 1, 1])

        for p in tf:
            color_tf.AddRGBPoint(p[0], p[1], p[2], p[3])
            opacity_tf.AddPoint(p[0], p[4])

        # working on the CPU - create mapping - use RayCasting
        volMapper = vtk.vtkVolumeRayCastMapper()
        compositeFunction = vtk.vtkVolumeRayCastCompositeFunction()
        compositeFunction.SetCompositeMethodToInterpolateFirst()
        volMapper.SetVolumeRayCastFunction(compositeFunction)
        volMapper.SetInputConnection(importer.GetOutputPort())

        # the property describes how the data will look
        self.volProperty = vtk.vtkVolumeProperty()
        self.volProperty.SetColor(color_tf)
        self.volProperty.SetScalarOpacity(opacity_tf)
        self.volProperty.ShadeOn()
        self.volProperty.SetInterpolationTypeToLinear()

        volume = vtk.vtkVolume()
        volume.SetMapper(volMapper)
        volume.SetProperty(self.volProperty)

        return volume