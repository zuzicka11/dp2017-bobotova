import vtk
from vtk.util.misc import vtkGetDataRoot
from PyQt4 import QtGui

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

# create 2D slices actors for visualization
class Slicer():

    def __init__(self, modelWindow, modelRenderer, axe):
        self.resliceBrain = vtk.vtkImageReslice()
        self.resliceTumor = vtk.vtkImageReslice()
        self.table = vtk.vtkLookupTable()
        self.modelWindow = modelWindow
        self.modelRenderer = modelRenderer
        self.axes = axe

    # get slices for brain visualization
    def getBrainSlices(self, sliceType, brain_path):
        VTK_DATA_ROOT = vtkGetDataRoot()

        # start by loading some data
        reader = vtk.vtkMetaImageReader()
        reader.SetFileName(brain_path)
        reader.Update()

        # calculate the center of the volume
        reader.Update()
        (xMin, xMax, yMin, yMax, zMin, zMax) = reader.GetExecutive().GetWholeExtent(reader.GetOutputInformation(0))
        (xSpacing, ySpacing, zSpacing) = reader.GetOutput().GetSpacing()
        (x0, y0, z0) = reader.GetOutput().GetOrigin()

        center = [x0 + xSpacing * 0.5 * (xMin + xMax),
                  y0 + ySpacing * 0.5 * (yMin + yMax),
                  z0 + zSpacing * 0.5 * (zMin + zMax)]

        metrix = vtk.vtkMatrix4x4()
        # matrices for axial, coronal, sagittal, oblique view orientations
        if sliceType == "coronal":
            metrix.DeepCopy((1, 0, 0, center[0],
                              0, 0, 1, center[1],
                              0, 1, 0, center[2],
                              0, 0, 0, 1))

            self.axes.paintAxeZ([center[0], - center[1], 0], [center[0], - center[1], 250], self.modelRenderer)
            self.axes.setCenterZ(center[2])

        elif sliceType == "sagittal":
            metrix.DeepCopy((0, 0,-1, center[0],
                               1, 0, 0, center[1],
                               0, 1, 0, center[2],
                               0, 0, 0, 1))
            self.axes.paintAxeY([center[0], 0, center[2]], [center[0], 250, center[2]], self.modelRenderer)
            self.axes.setCenterY(-center[1])

        elif sliceType == "oblique":
            metrix.DeepCopy((1, 0, 0, center[0],
                              0, 0.866025, -0.5, center[1],
                              0, 0.5, 0.866025, center[2],
                              0, 0, 0, 1))
            self.line = self.axeObject.paintLine([0, 0, 0], [0, 0, 0])

        else:  # axial
            metrix.DeepCopy((1, 0, 0, center[0],
                            0, 1, 0, center[1],
                            0, 0, 1, center[2],
                            0, 0, 0, 1))
            self.axes.paintAxeX([0, - center[1], center[2]], [250, - center[1], center[2]], self.modelRenderer)
            self.axes.setCenterX(center[0])

        self.modelWindow.Render()

        # extract a slice in the desired orientation
        self.resliceBrain.SetInputConnection(reader.GetOutputPort())
        self.resliceBrain.SetOutputDimensionality(2)
        self.resliceBrain.SetResliceAxes(metrix)
        self.resliceBrain.SetInterpolationModeToLinear()

        # create a greyscale lookup table
        self.table.SetRange(0, 2000) # image intensity range
        self.table.SetValueRange(0.0, 1.0) # from black to white
        self.table.SetSaturationRange(0.0, 0.0) # no color saturation
        self.table.SetRampToLinear()
        self.table.Build()

        # map the image through the lookup table
        color = vtk.vtkImageMapToColors()
        color.SetLookupTable(self.table)
        color.SetInputConnection(self.resliceBrain.GetOutputPort())

        threshold = vtk.vtkThreshold()
        threshold.SetInputConnection(color.GetOutputPort())
        threshold.ThresholdByUpper(100)

        # display the image
        actor = vtk.vtkImageActor()
        actor.GetMapper().SetInputConnection(color.GetOutputPort())

        return actor

    def setMinValue(self, minValue, window):
        self.table.SetRange(minValue, 2000)
        window.Render()

    # get slices for tumor visualization
    def getTumorSlices(self, sliceType, segmentation_path):
        VTK_DATA_ROOT = vtkGetDataRoot()

        # start by loading some data.

        reader = vtk.vtkMetaImageReader()
        reader.SetFileName(segmentation_path)
        reader.Update()

        # calculate the center of the volume
        reader.Update()
        (xMin, xMax, yMin, yMax, zMin, zMax) = reader.GetExecutive().GetWholeExtent(reader.GetOutputInformation(0))
        (xSpacing, ySpacing, zSpacing) = reader.GetOutput().GetSpacing()
        (x0, y0, z0) = reader.GetOutput().GetOrigin()
        print(xMin, xMax, yMin, yMax, zMin, zMax)
        center = [x0 + xSpacing * 0.5 * (xMin + xMax),
                  y0 + ySpacing * 0.5 * (yMin + yMax),
                  z0 + zSpacing * 0.5 * (zMin + zMax)]


        metrix = vtk.vtkMatrix4x4()
        # matrices for axial, coronal, sagittal, oblique view orientations
        if sliceType == "coronal":
            metrix.DeepCopy((1, 0, 0, center[0],
                              0, 0, 1, center[1],
                              0,-1, 0, center[2],
                              0, 0, 0, 1))
        elif sliceType == "sagittal":
            metrix.DeepCopy((0, 0,-1, center[0],
                               1, 0, 0, center[1],
                               0, 1, 0, center[2],
                               0, 0, 0, 1))
        elif sliceType == "oblique":
            metrix.DeepCopy((1, 0, 0, center[0],
                              0, 0.866025, -0.5, center[1],
                              0, 0.5, 0.866025, center[2],
                              0, 0, 0, 1))
        else:  # axial
            metrix.DeepCopy((1, 0, 0, center[0],
                            0, 1, 0, center[1],
                            0, 0, 1, center[2],
                            0, 0, 0, 1))

        # extract a slice in the desired orientation
        self.resliceTumor.SetInputConnection(reader.GetOutputPort())
        self.resliceTumor.SetOutputDimensionality(2)
        self.resliceTumor.SetResliceAxes(metrix)
        self.resliceTumor.SetInterpolationModeToLinear()

        # create a greyscale lookup table
        table = vtk.vtkLookupTable()
        table.SetRange(0, 4)  # image intensity range
        table.SetNumberOfColors(5)
        table.SetTableValue(0, 0.0, 0.0, 0.0, 0.0)
        table.SetTableValue(1, 0.2, 0.2, 0.2, 1.0)
        table.SetTableValue(2, 0.4, 0.4, 0.4, 1.0)
        table.SetTableValue(3, 0.8, 0.8, 0.8, 1.0)
        table.SetTableValue(4, 1.0, 1.0, 1.0, 1.0)
        table.SetRampToLinear()
        table.Build()

        # map the image through the lookup table
        color = vtk.vtkImageMapToColors()
        color.SetLookupTable(table)
        color.SetInputConnection(self.resliceTumor.GetOutputPort())

        threshold = vtk.vtkThreshold()
        threshold.SetInputConnection(color.GetOutputPort())
        threshold.ThresholdByUpper(100)

        # display the image
        actor = vtk.vtkImageActor()
        actor.GetMapper().SetInputConnection(color.GetOutputPort())

        return actor

    # set interaction to slide throught slices
    def setInteraction(self, interactor, window, label):
        self.interactor = interactor
        self.window = window
        self.label = label
        self.interactorStyle = vtk.vtkInteractorStyleTrackballCamera()
        self.actions = {}
        self.actions["Slicing"] = 0
        slice_type = -1

        self.interactorStyle.AddObserver("MouseMoveEvent", self.MouseMoveCallback)
        self.interactorStyle.AddObserver("LeftButtonPressEvent", self.ButtonCallback)
        self.interactorStyle.AddObserver("LeftButtonReleaseEvent", self.ButtonCallback)

        interactor.SetInteractorStyle(self.interactorStyle)

    def MouseMoveCallback(self, obj, event):
        (lastX, lastY) = self.interactor.GetLastEventPosition()
        (mouseX, mouseY) = self.interactor.GetEventPosition()

        if self.actions["Slicing"] == 1:
            deltaY = mouseY - lastY
            self.resliceBrain.Update()
            sliceSpacing = self.resliceBrain.GetOutput().GetSpacing()[2]
            matrix = self.resliceBrain.GetResliceAxes()

            # move the center point that we are slicing through
            center = matrix.MultiplyPoint((0, 0, sliceSpacing * deltaY, 1))
            matrix.SetElement(0, 3, center[0])
            matrix.SetElement(1, 3, center[1])
            matrix.SetElement(2, 3, center[2])

            self.resliceTumor.Update()
            sliceSpacing = self.resliceTumor.GetOutput().GetSpacing()[2]
            matrix = self.resliceTumor.GetResliceAxes()

            # move the center point that we are slicing through
            center = matrix.MultiplyPoint((0, 0, sliceSpacing * deltaY, 1))
            matrix.SetElement(0, 3, center[0])
            matrix.SetElement(1, 3, center[1])
            matrix.SetElement(2, 3, center[2])

            self.window.Render()

            # paint slice deending on type, move axe in volume on reslicing, show slice number
            if(self.label.text().startswith("Axial")):
                self.label.setText(_translate("MainWindow", "Axial: " + str(int(center[2])), None))
                self.axes.setCenterZ(center[2])
                if self.axes.getDisplay():
                    self.axes.paintAxeX([0, self.axes.getCenterY(), self.axes.getCenterZ()], [250, self.axes.getCenterY(), self.axes.getCenterZ()], self.modelRenderer)
                    self.axes.paintAxeY([self.axes.getCenterX(), 0, self.axes.getCenterZ()], [self.axes.getCenterX(), 250, self.axes.getCenterZ()], self.modelRenderer)
                    self.modelWindow.Render()

            if (self.label.text().startswith("Coronal")):
                self.label.setText(_translate("MainWindow", "Coronal: " + str(int(center[1])), None))
                self.axes.setCenterY(center[1])
                if self.axes.getDisplay():
                    self.axes.paintAxeX([0, center[1], self.axes.getCenterZ()], [250, center[1], self.axes.getCenterZ()], self.modelRenderer)
                    self.axes.paintAxeZ([self.axes.getCenterX(), self.axes.getCenterY(), 0], [self.axes.getCenterX(), self.axes.getCenterY(), 250], self.modelRenderer)
                    self.modelWindow.Render()

            if (self.label.text().startswith("Sagittal")):
                self.label.setText(_translate("MainWindow", "Sagittal: " + str(int(center[0])), None))
                self.axes.setCenterX(center[0])
                if self.axes.getDisplay():
                    self.axes.paintAxeY([self.axes.getCenterX(), 0, self.axes.getCenterZ()], [self.axes.getCenterX(), 250, self.axes.getCenterZ()], self.modelRenderer)
                    self.axes.paintAxeZ([self.axes.getCenterX(), self.axes.getCenterY(), 0], [self.axes.getCenterX(), self.axes.getCenterY(), 250], self.modelRenderer)
                    self.modelWindow.Render()

        else:
            self.interactorStyle.OnMouseMove()


    def ButtonCallback(self, obj, event):
        if event == "LeftButtonPressEvent":
            self.actions["Slicing"] = 1
        else:
            self.actions["Slicing"] = 0
