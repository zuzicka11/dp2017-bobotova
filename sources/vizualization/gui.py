# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):

        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1850, 1020)

        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

        # set font size for widget labels and headers of menu
        fontSize14 = QtGui.QFont()
        fontSize14.setPointSize(14)

        # axial widget
        self.axialWidget = QtGui.QWidget(self.centralwidget)
        self.axialWidget.setGeometry(QtCore.QRect(500, 30, 641, 451))
        self.axialWidget.setObjectName(_fromUtf8("axialWidget"))
        gridlayout = QtGui.QGridLayout(self.axialWidget)
        self.axialvtkWidget = QVTKRenderWindowInteractor(self.axialWidget)
        gridlayout.addWidget(self.axialvtkWidget, 0, 0, 1, 1)
        # axial label
        self.axialLabel = QtGui.QLabel(self.centralwidget)
        self.axialLabel.setGeometry(QtCore.QRect(500, 10, 211, 21))
        self.axialLabel.setFont(fontSize14)
        self.axialLabel.setObjectName(_fromUtf8("axialLabel"))

        # sagittal widget
        self.sagittalWidget = QtGui.QWidget(self.centralwidget)
        self.sagittalWidget.setGeometry(QtCore.QRect(500, 510, 641, 451))
        self.sagittalWidget.setObjectName(_fromUtf8("sagittalWidget"))
        gridlayout = QtGui.QGridLayout(self.sagittalWidget)
        self.sagittalvtkWidget = QVTKRenderWindowInteractor(self.sagittalWidget)
        gridlayout.addWidget(self.sagittalvtkWidget, 0, 0, 1, 1)
        # sagittal label
        self.sagittalLabel = QtGui.QLabel(self.centralwidget)
        self.sagittalLabel.setGeometry(QtCore.QRect(500, 490, 241, 21))
        self.sagittalLabel.setFont(fontSize14)
        self.sagittalLabel.setObjectName(_fromUtf8("sagittalLabel"))

        # oblique widget
        self.obliqueWidget = QtGui.QWidget(self.centralwidget)
        self.obliqueWidget.setGeometry(QtCore.QRect(1170, 510, 641, 451))
        self.obliqueWidget.setObjectName(_fromUtf8("obliqueWidget"))
        gridlayout = QtGui.QGridLayout(self.obliqueWidget)
        self.obliquevtkWidget = QVTKRenderWindowInteractor(self.obliqueWidget)
        gridlayout.addWidget(self.obliquevtkWidget, 0, 0, 1, 1)
        # oblique label
        self.obliqueLabel = QtGui.QLabel(self.centralwidget)
        self.obliqueLabel.setGeometry(QtCore.QRect(1170, 490, 221, 21))
        self.obliqueLabel.setFont(fontSize14)
        self.obliqueLabel.setObjectName(_fromUtf8("obliqueLabel"))

        # model widget
        self.modelWidget = QtGui.QWidget(self.centralwidget)
        self.modelWidget.setGeometry(QtCore.QRect(1170, 30, 641, 451))
        self.modelWidget.setObjectName(_fromUtf8("model"))
        gridlayout = QtGui.QGridLayout(self.modelWidget)
        self.modelvtkWidget = QVTKRenderWindowInteractor(self.modelWidget)
        gridlayout.addWidget(self.modelvtkWidget, 0, 0, 1, 1)
        # model label
        self.modelLabel = QtGui.QLabel(self.centralwidget)
        self.modelLabel.setGeometry(QtCore.QRect(1170, 10, 191, 21))
        self.modelLabel.setFont(fontSize14)
        self.modelLabel.setObjectName(_fromUtf8("modelLabel"))


        # MENU

        # 3D model part
        # header label
        self.menuModelLabel = QtGui.QLabel(self.centralwidget)
        self.menuModelLabel.setGeometry(QtCore.QRect(20, 20, 101, 21))
        self.menuModelLabel.setFont(fontSize14)
        self.menuModelLabel.setObjectName(_fromUtf8("menuModelLabel"))
        # brain checkbox
        self.menuModelBrainCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelBrainCheckbox.setGeometry(QtCore.QRect(40, 60, 99, 22))
        self.menuModelBrainCheckbox.setObjectName(_fromUtf8("menuModelBrainCheckbox"))
        # health brain checkbox
        self.menuModelHealthBrainCheckBox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelHealthBrainCheckBox.setGeometry(QtCore.QRect(60, 100, 111, 22))
        self.menuModelHealthBrainCheckBox.setObjectName(_fromUtf8("menuModelHealthBrainCheckBox"))
        # healt brain opacity label
        self.menuModelHealthBrainOpacityLabel = QtGui.QLabel(self.centralwidget)
        self.menuModelHealthBrainOpacityLabel.setGeometry(QtCore.QRect(80, 130, 100, 17))
        self.menuModelHealthBrainOpacityLabel.setObjectName(_fromUtf8("menuModelHealthBrainOpacityLabel"))
        # healt brain opacity slider
        self.menuModelHealthBrainOpacitySlider = QtGui.QSlider(self.centralwidget)
        self.menuModelHealthBrainOpacitySlider.setGeometry(QtCore.QRect(80, 150, 371, 29))
        self.menuModelHealthBrainOpacitySlider.setOrientation(QtCore.Qt.Horizontal)
        self.menuModelHealthBrainOpacitySlider.setObjectName(_fromUtf8("menuModelHealthBrainOpacitySlider"))
        # tumor checkbox
        self.menuModelTumorCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelTumorCheckbox.setGeometry(QtCore.QRect(60, 200, 99, 22))
        self.menuModelTumorCheckbox.setObjectName(_fromUtf8("menuSlicesTumorCheckbox"))
        # tumor in front checkbox
        self.menuModelTumorInFrontrCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelTumorInFrontrCheckbox.setGeometry(QtCore.QRect(180, 200, 99, 22))
        self.menuModelTumorInFrontrCheckbox.setObjectName(_fromUtf8("menuSlicesTumorCheckbox"))
        # tumor highlight checkbox
        self.menuModelTumorHighlightCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelTumorHighlightCheckbox.setGeometry(QtCore.QRect(270, 200, 99, 22))
        self.menuModelTumorHighlightCheckbox.setObjectName(_fromUtf8("menuSlicesTumorCheckbox"))
        # tumor highlight checkbox
        self.menuModelTumorRecolorCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelTumorRecolorCheckbox.setGeometry(QtCore.QRect(370, 200, 99, 22))
        self.menuModelTumorRecolorCheckbox.setObjectName(_fromUtf8("menuSlicesTumorCheckbox"))
        # tumor opacity label
        self.menuModelTumorOpacityLabel = QtGui.QLabel(self.centralwidget)
        self.menuModelTumorOpacityLabel.setGeometry(QtCore.QRect(80, 230, 100, 17))
        self.menuModelTumorOpacityLabel.setObjectName(_fromUtf8("menuSlicesTumorOpacityLabel"))
        # tumor opacity slider
        self.menuModelTumorOpacitySlider = QtGui.QSlider(self.centralwidget)
        self.menuModelTumorOpacitySlider.setGeometry(QtCore.QRect(80, 250, 371, 29))
        self.menuModelTumorOpacitySlider.setOrientation(QtCore.Qt.Horizontal)
        self.menuModelTumorOpacitySlider.setObjectName(_fromUtf8("menuSlicesTumorOpacitySlider"))
        # axes checkbox
        self.menuModelAxesCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuModelAxesCheckbox.setGeometry(QtCore.QRect(60, 300, 99, 22))
        self.menuModelAxesCheckbox.setObjectName(_fromUtf8("menuModelAxesCheckbox"))

        # slices part
        # header label
        self.menuSlicesLabel = QtGui.QLabel(self.centralwidget)
        self.menuSlicesLabel.setGeometry(QtCore.QRect(20, 350, 68, 17))
        self.menuSlicesLabel.setFont(fontSize14)
        self.menuSlicesLabel.setObjectName(_fromUtf8("menuSlicesLabel"))
        # show boundaries checkbox
        self.menuSlicesShowBoundariesCheckbox = QtGui.QCheckBox(self.centralwidget)
        self.menuSlicesShowBoundariesCheckbox.setGeometry(QtCore.QRect(40, 380, 211, 22))
        self.menuSlicesShowBoundariesCheckbox.setObjectName(_fromUtf8("menuSlicesShowBoundariesCheckbox"))
        # show values greater than label
        self.menuSlicesShowValuesGTLabel = QtGui.QLabel(self.centralwidget)
        self.menuSlicesShowValuesGTLabel.setGeometry(QtCore.QRect(40, 420, 300, 17))
        self.menuSlicesShowValuesGTLabel.setObjectName(_fromUtf8("menuSlicesShowValuesGTLabel"))
        # show values greater than slider
        self.menuSlicesShowValueGTSlider = QtGui.QSlider(self.centralwidget)
        self.menuSlicesShowValueGTSlider.setGeometry(QtCore.QRect(40, 440, 411, 29))
        self.menuSlicesShowValueGTSlider.setOrientation(QtCore.Qt.Horizontal)
        self.menuSlicesShowValueGTSlider.setObjectName(_fromUtf8("menuSlicesShowValueGTSlider"))


        # histogram part
        # histogram label
        self.menuHistogramHBLabel = QtGui.QLabel(self.centralwidget)
        self.menuHistogramHBLabel.setGeometry(QtCore.QRect(20, 500, 300, 21))
        self.menuHistogramHBLabel.setFont(fontSize14)
        self.menuHistogramHBLabel.setObjectName(_fromUtf8("menuHistogramLabel"))
        # histogram widget
        self.menuHistogramHBWidget = QtGui.QWidget(self.centralwidget)
        self.menuHistogramHBWidget.setGeometry(QtCore.QRect(20, 530, 460, 200))
        self.menuHistogramHBWidget.setObjectName(_fromUtf8("menuHistogramWidget"))
        gridlayout = QtGui.QGridLayout(self.menuHistogramHBWidget)
        self.histogramHBvtkWidget = QVTKRenderWindowInteractor(self.menuHistogramHBWidget)
        gridlayout.addWidget(self.histogramHBvtkWidget, 0, 0, 1, 1)
        # histogram label
        self.menuHistogramTLabel = QtGui.QLabel(self.centralwidget)
        self.menuHistogramTLabel.setGeometry(QtCore.QRect(20, 750, 300, 21))
        self.menuHistogramTLabel.setFont(fontSize14)
        self.menuHistogramTLabel.setObjectName(_fromUtf8("menuHistogramLabel"))
        # histogram widget
        self.menuHistogramTWidget = QtGui.QWidget(self.centralwidget)
        self.menuHistogramTWidget.setGeometry(QtCore.QRect(20, 780, 460, 200))
        self.menuHistogramTWidget.setObjectName(_fromUtf8("menuHistogramWidget"))
        gridlayout = QtGui.QGridLayout(self.menuHistogramTWidget)
        self.histogramTvtkWidget = QVTKRenderWindowInteractor(self.menuHistogramTWidget)
        gridlayout.addWidget(self.histogramTvtkWidget, 0, 0, 1, 1)


        # other parts - not actively used yet

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1156, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        # set window name
        MainWindow.setWindowTitle(_translate("MainWindow", "Brain tumor detection", None))

        # set widget label names
        self.axialLabel.setText(_translate("MainWindow", "Axial", None))
        self.sagittalLabel.setText(_translate("MainWindow", "Sagittal", None))
        self.obliqueLabel.setText(_translate("MainWindow", "Coronal", None))
        self.modelLabel.setText(_translate("MainWindow", "3D model", None))

        # set menu labels names
        # 3D model part
        self.menuModelLabel.setText(_translate("MainWindow", "3D model", None))
        self.menuModelBrainCheckbox.setText(_translate("MainWindow", "brain", None))
        self.menuModelHealthBrainCheckBox.setText(_translate("MainWindow", "health brain", None))
        self.menuModelHealthBrainOpacityLabel.setText(_translate("MainWindow", "opacity: 7%", None))
        self.menuModelTumorCheckbox.setText(_translate("MainWindow", "tumor", None))
        self.menuModelTumorHighlightCheckbox.setText(_translate("MainWindow", "highlight", None))
        self.menuModelTumorRecolorCheckbox.setText(_translate("MainWindow", "recolor", None))
        self.menuModelTumorInFrontrCheckbox.setText(_translate("MainWindow", "in front", None))
        self.menuModelTumorOpacityLabel.setText(_translate("MainWindow", "opacity: 100%", None))
        self.menuModelAxesCheckbox.setText(_translate("MainWindow", "axes", None))
        self.menuModelBrainCheckbox.setChecked(True)
        self.menuModelHealthBrainCheckBox.setChecked(True)
        self.menuModelTumorCheckbox.setChecked(True)
        self.menuModelAxesCheckbox.setChecked(True)
        # slices part
        self.menuSlicesLabel.setText(_translate("MainWindow", "Slices", None))
        self.menuSlicesShowBoundariesCheckbox.setText(_translate("MainWindow", "show tumor  mask", None))
        self.menuSlicesShowValuesGTLabel.setText(_translate("MainWindow", "Show values equal or greater than: 0", None))
        # histogram part
        self.menuHistogramHBLabel.setText(_translate("MainWindow", "Health brain histogram (10 bins)", None))
        self.menuHistogramTLabel.setText(_translate("MainWindow", "Tumor histogram (10 bins)", None))

        # set sliders ranges
        self.menuSlicesShowValueGTSlider.setRange(0, 1000)
        self.menuModelTumorOpacitySlider.setRange(0, 100)
        self.menuModelTumorOpacitySlider.setValue(100)
        self.menuModelHealthBrainOpacitySlider.setRange(0, 100)
        self.menuModelHealthBrainOpacitySlider.setValue(7)





