import vtk
from vizualization import axe


# paint create histogram for visualization
class Histogram():

    def makeHistogram(self, data):
        print(data)

        min = 100000
        max = 0

        self.points = vtk.vtkPoints()
        polygons = vtk.vtkCellArray()

        for idx, d in enumerate(data):
            if idx > 0:
                if d < min:
                    min = d
                if d > max:
                    max = d

        for idx, d in enumerate(data):
            if(idx > 0):
                print(d)
                d = (d - min)/(max-min)
                print(d)
                polygons.InsertNextCell(self.createRectangle((idx-1)*0.1, (idx-1)*0.1+0.1, 0, d, (idx-1)*4))

        # polygons.InsertNextCell(self.createRectangle(10, 20, 0, 40, 4))

        polygonPolyData = vtk.vtkPolyData()
        polygonPolyData.SetPoints(self.points)
        polygonPolyData.SetPolys(polygons)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(polygonPolyData)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)

        axeObject = axe.Axe()
        actorX = axeObject.paintLine([0, 0, 0], [len(data)*0.1, 0, 0])
        actorY = axeObject.paintLine([0, 0, 0], [0, 1, 0])

        actorList = []
        actorList.append(actor)
        actorList.append(actorX)
        actorList.append(actorY)

        return actorList

    def createRectangle(self, x1, x2, y1, y2, i):
        self.points.InsertNextPoint(x1, y1, 0)
        self.points.InsertNextPoint(x1, y2, 0)
        self.points.InsertNextPoint(x2, y2, 0)
        self.points.InsertNextPoint(x2, y1, 0)

        polygon = vtk.vtkPolygon()
        polygon.GetPointIds().SetNumberOfIds(4)
        polygon.GetPointIds().SetId(0, i)
        polygon.GetPointIds().SetId(1, i+1)
        polygon.GetPointIds().SetId(2, i+2)
        polygon.GetPointIds().SetId(3, i+3)

        return polygon