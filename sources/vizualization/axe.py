import vtk


# paint axes to the 3D visualization
class Axe:

    def __init__(self):
        self.x = vtk.vtkActor()
        self.y = vtk.vtkActor()
        self.z = vtk.vtkActor()
        self.centerX = 0
        self.centerY = 0
        self.centerZ = 0
        self.display = True


    def getDisplay(self):
        return self.display

    def setDisplay(self, d):
        self.display = d

    def paintAxeX(self, x1, x2, renderer):
        renderer.RemoveActor(self.x)
        self.x = self.paintLine(x1, x2)
        renderer.AddActor(self.x)

    def getAxeX(self):
        return self.x

    def paintAxeY(self, y1, y2, renderer):
        renderer.RemoveActor(self.y)
        self.y = self.paintLine(y1, y2)
        renderer.AddActor(self.y)

    def getAxeY(self):
        return self.y

    def paintAxeZ(self, z1, z2, renderer):
        renderer.RemoveActor(self.z)
        self.z = self.paintLine(z1, z2)
        renderer.AddActor(self.z)

    def getAxeZ(self):
        return self.z

    def getCenterX(self):
        return self.centerX

    def setCenterX(self, c):
        self.centerX = c

    def getCenterY(self):
        return self.centerY

    def setCenterY(self, c):
        self.centerY = c

    def getCenterZ(self):
        return self.center

    def setCenterZ(self, c):
        self.center = c

    def paintLine(self, x1, x2):

        # Create a vtkPoints object and store the points in it
        pts = vtk.vtkPoints()
        pts.InsertNextPoint(x1)
        pts.InsertNextPoint(x2)

        # Setup two colors - one for each line
        green = [0, 255, 0]

        # Setup the colors array
        colors = vtk.vtkUnsignedCharArray()
        colors.SetNumberOfComponents(3)
        colors.SetName("Colors")

        # Add the colors we created to the colors array
        colors.InsertNextTuple(green)

        # Create the x line
        linex = vtk.vtkLine()
        linex.GetPointIds().SetId(0, 0)
        linex.GetPointIds().SetId(1, 1)

        # Create a cell array to store the lines in and add the lines to it
        lines = vtk.vtkCellArray()
        lines.InsertNextCell(linex)

        # Create a polydata to store everything in
        linesPolyData = vtk.vtkPolyData()

        # Add the points to the dataset
        linesPolyData.SetPoints(pts)

        # Add the lines to the dataset
        linesPolyData.SetLines(lines)

        # Color the lines - associate the first component (red) of the
        # colors array with the first component of the cell array (line 0)
        # and the second component (green) of the colors array with the
        # second component of the cell array (line 1)
        linesPolyData.GetCellData().SetScalars(colors)

        # Visualize
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(linesPolyData)

        line_actor = vtk.vtkActor()
        line_actor.SetMapper(mapper)

        return line_actor