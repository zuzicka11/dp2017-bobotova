import cv2
import os
import numpy as np
import numpngw

path = '../../data/rigid_dataset/'

# correction of dataset - remove absurd white points
for (path, dirs, files) in os.walk(path):
    for file in files:
        if "axial_processed" in file:

            my_file = os.path.join(path, file)

            print(my_file)

            image = cv2.imread(my_file, -1)
            cv2.imshow("image", image)

            if len(np.where(image > 65000)):
                image[np.where(image > 65200)] = 0
                numpngw.write_png(my_file, np.uint16(image))
                print(" - oprava")

            cv2.imshow("image correct", image)

            cv2.waitKey(100)