import cv2
import numpy as np
import math
from matplotlib import pyplot as plt
import os

# function can compare after patches using correlation, intersection and bhattacharyya metrics
def compare_images(img_1, img_2, path, diff, save_results=False):

    # cv2.imshow("img 1", img_1)
    # cv2.imshow("img 2", img_2)

    count_of_rows = 10
    count_of_cols = 8

    cell_Height = math.floor(img_1.shape[0] / count_of_rows)
    cell_Width = math.floor(img_1.shape[1] / count_of_cols)
    number_of_pixels_in_section = cell_Height*cell_Width

    hist_correlation = []
    hist_intersection = []
    hist_bhattacharyya = []

    # for all rectangular patches compute metrics
    for i in range(0, img_1.shape[0]-cell_Height, cell_Height):
        for j in range(0, img_1.shape[1]-cell_Width, cell_Width):

            img_1_section = img_1[i:i+cell_Height, j:j+cell_Width]
            img_2_section = img_2[i:i+cell_Height, j:j+cell_Width]

            img_1_copy = img_1.copy()
            img_2_copy = img_2.copy()

            cv2.rectangle(img=img_1_copy, pt1=(j, i), pt2=(j + cell_Width, i + cell_Height), color=(255, 255, 255))
            cv2.rectangle(img=img_2_copy, pt1=(j, i), pt2=(j + cell_Width, i + cell_Height), color=(255, 255, 255))

            if save_results:
                cv2.imwrite(path+"/viz_img_1_"+diff+"_section_"+str(i)+"_"+str(j)+".png", img_1_copy)
                cv2.imwrite(path+"/viz_img_2_"+diff+"_section_"+str(i)+"_"+str(j)+".png", img_2_copy)

            if len(img_1_section[np.where(img_1_section > 0)]) > (number_of_pixels_in_section/2) or len(img_2_section[np.where(img_2_section > 0)]) > (number_of_pixels_in_section/2):
                hist_1 = cv2.calcHist([img_1_section], [0], None, [256], [0, 256])
                hist_2 = cv2.calcHist([img_2_section], [0], None, [256], [0, 256])

                if save_results:
                    plt.figure()
                    plt.rcParams['axes.grid'] = True
                    plt.plot(hist_1)
                    plt.plot(hist_2)
                    plt.savefig(path+"/viz_img_1_"+diff+"_section_hist_"+str(i)+"_"+str(j)+".png")
                    plt.close()


                hist_correlation.append(cv2.compareHist(hist_1, hist_2, cv2.HISTCMP_CORREL))
                hist_intersection.append(cv2.compareHist(hist_1, hist_2, cv2.HISTCMP_INTERSECT))
                hist_bhattacharyya.append(cv2.compareHist(hist_1, hist_2, cv2.HISTCMP_BHATTACHARYYA))

    # compute average metrics of patches
    correlation = sum(hist_correlation)/len(hist_correlation)
    intersection = sum(hist_intersection)/len(hist_intersection)
    bhattacharyya = sum(hist_bhattacharyya)/len(hist_bhattacharyya)

    return correlation, intersection, bhattacharyya