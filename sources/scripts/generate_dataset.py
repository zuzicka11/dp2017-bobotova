import numpy as np
import numpngw
import random
import cv2
import os

from logic.preprocessing import Preprocessing
from logic.reader import Reader

def rotate(img, angle):
    # rotation
    rows, cols = img.shape
    print("-rotation angle:", angle)
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
    img = cv2.warpAffine(img, M, (cols, rows))

    return img

def scale(img, scale_val):
    # scaling
    rows, cols = img.shape
    img = cv2.resize(img, None, fx=scale_val, fy=scale_val, interpolation=cv2.INTER_CUBIC)
    new_rows, new_cols = img.shape
    top = bottom = (rows - new_rows) / 2
    left = right = (cols - new_cols) / 2
    if new_rows+int(top)+int(bottom) < rows:
        bottom += 1
    if new_cols + int(left) + int(right) < cols:
        right += 1
    print("-scale value:", scale_val, "old rowsXcols:", rows, "x", cols, "new rowsXcols:", new_rows, "x", new_cols, "margin T-B-L-R:", int(top), "-", int(bottom), "-", int(left), "-", int(right), "with margin rowsXcols:", new_rows+int(top)+int(bottom), "x", new_cols+int(left)+int(right))
    img = cv2.copyMakeBorder(img, top=int(top), bottom=int(round(bottom)), left=int(left), right=int(round(right)), borderType=cv2.BORDER_CONSTANT, value=0)

    return img

def translate(img, x, y):
    # translation
    rows, cols = img.shape

    print("-translate x and y:", x, "and", y)

    M = np.float32([[1, 0, x], [0, 1, y]])
    img = cv2.warpAffine(img, M, (cols, rows))

    return img

paths_to_patients = [
    # "../../data/rigid_dataset/subj_1",
    # "../../data/rigid_dataset/subj_3",
    # "../../data/rigid_dataset/subj_6",
    # "../../data/rigid_dataset/subj_7",
    # "../../data/rigid_dataset/subj_9",
    # "../../data/rigid_dataset/subj_10",
    # "../../data/rigid_dataset/subj_12",
    # "../../data/rigid_dataset/subj_13",
    # "../../data/rigid_dataset/subj_16",
    # "../../data/rigid_dataset/subj_19",
    # "../../data/rigid_dataset/subj_20",
    # "../../data/rigid_dataset/subj_21",
    # "../../data/rigid_dataset/subj_25",
    # "../../data/rigid_dataset/subj_26",
    # "../../data/rigid_dataset/subj_27",
    # "../../data/rigid_dataset/subj_30",
    # "../../data/rigid_dataset/subj_32",
    # "../../data/rigid_dataset/subj_33",
    # "../../data/rigid_dataset/subj_34",
    # "../../data/rigid_dataset/subj_36",
    # "../../data/rigid_dataset/subj_38",
    # "../../data/rigid_dataset/subj_40",
    # "../../data/rigid_dataset/subj_41",
    # "../../data/rigid_dataset/subj_42",
    # "../../data/rigid_dataset/subj_45",
    # "../../data/rigid_dataset/subj_46",
    # "../../data/rigid_dataset/subj_48",
    # "../../data/rigid_dataset/subj_49",
    # "../../data/rigid_dataset/subj_50",
    # "../../data/rigid_dataset/subj_55",
    # "../../data/rigid_dataset/subj_56",
    # "../../data/rigid_dataset/subj_58",
    # "../../data/rigid_dataset/subj_59",
    # "../../data/rigid_dataset/subj_62",
    # "../../data/rigid_dataset/subj_66",
    # "../../data/rigid_dataset/subj_68",
    # "../../data/rigid_dataset/subj_71",
    # "../../data/rigid_dataset/subj_73",
    # "../../data/rigid_dataset/subj_74",
    # "../../data/rigid_dataset/subj_75",
    # "../../data/rigid_dataset/subj_79",
    # "../../data/rigid_dataset/subj_82",
    # "../../data/rigid_dataset/subj_83",
    # "../../data/rigid_dataset/subj_84",
    # "../../data/rigid_dataset/subj_86",
    # "../../data/rigid_dataset/subj_89",
    # "../../data/rigid_dataset/subj_92",
    # "../../data/rigid_dataset/subj_93",
    # "../../data/rigid_dataset/subj_95",
    # "../../data/rigid_dataset/subj_100",
    # "../../data/rigid_dataset/subj_106",
    # "../../data/rigid_dataset/subj_107",
    # "../../data/rigid_dataset/subj_108",
    # "../../data/rigid_dataset/subj_114",
    # "../../data/rigid_dataset/subj_117",
    # "../../data/rigid_dataset/subj_119",
    # "../../data/rigid_dataset/subj_120",
    # "../../data/rigid_dataset/subj_122",
    # "../../data/rigid_dataset/subj_126",
    # "../../data/rigid_dataset/subj_127",
    # "../../data/rigid_dataset/subj_128",
    # "../../data/rigid_dataset/subj_130",
    # "../../data/rigid_dataset/subj_131",
    # "../../data/rigid_dataset/subj_134",
    # "../../data/rigid_dataset/subj_136",
    # "../../data/rigid_dataset/subj_137",
    # "../../data/rigid_dataset/subj_141",
    # "../../data/rigid_dataset/subj_142",
    # "../../data/rigid_dataset/subj_143",
    # "../../data/rigid_dataset/subj_145",
    # "../../data/rigid_dataset/subj_146",
    # "../../data/rigid_dataset/subj_148",
    # "../../data/rigid_dataset/subj_149",
    "../../data/rigid_dataset/subj_151",
    "../../data/rigid_dataset/subj_155",
    "../../data/rigid_dataset/subj_158",
    "../../data/rigid_dataset/subj_159",
    "../../data/rigid_dataset/subj_161",
    "../../data/rigid_dataset/subj_165"
]

# generate rigid dataset of 2D slices form volumes using afine transformations
for path_to_patient in paths_to_patients:

    patient_examinations_paths = [f.path for f in os.scandir(path_to_patient) if f.is_dir()]
    patient_examinations = []
    reader = Reader()

    for path in patient_examinations_paths:
        patient_examinations.append(reader.read(patient_examination_path=path))

    patient_examinations.sort(key=lambda x: x.name, reverse=False)

    print("CESTA", path_to_patient, "- pocet examinations:", len(patient_examinations))
    preprocessing = None

    for idx, examination in enumerate(patient_examinations):

        if len(examination.mri_flair.data) == len(examination.segmentation.data):

            if preprocessing is None:
                print("TEMPLATE pre preprocessing je", examination.name)
                preprocessing = Preprocessing(template=examination.mri_flair.data.copy(), save_results=False, path=examination.path)
                processed_volume = preprocessing.get_processed_teplate()
            else:
                print("Preprocesujem", examination.name)
                processed_volume = preprocessing.process_data(examination.mri_flair.data, path=examination.path)

            volume = examination.mri_flair
            volume_seg = examination.segmentation
            patient_exam_folder = examination.path

            print("EXAMINATION", examination.name)


            for index, slice in enumerate(volume.data):
                print("slice cislo", index)

                margin = 20
                slice = cv2.copyMakeBorder(slice, top=margin, bottom=margin, left=margin, right=margin, borderType=cv2.BORDER_CONSTANT, value=0)
                original = slice.copy()

                preprocessed_slice = cv2.copyMakeBorder(processed_volume[index], top=margin, bottom=margin, left=margin, right=margin, borderType=cv2.BORDER_CONSTANT, value=0)
                original_preprocessed = preprocessed_slice.copy()

                seg_slice = cv2.copyMakeBorder(volume_seg.data[index], top=margin, bottom=margin, left=margin, right=margin, borderType=cv2.BORDER_CONSTANT, value=0)
                seg_slice[np.where(seg_slice == 1)] = 65535
                seg_slice = np.uint16(seg_slice)
                original_seg = seg_slice.copy()

                if len(slice[np.where(slice == 0)]) < (slice.shape[0]*slice.shape[1]) - 20000:

                    if np.amin(slice) < 0:
                        print("- pocet odstranenych zapornych hodnot: ", len(slice[np.where(slice < 0)]))
                        slice[np.where(slice < 0)] = 0

                    # select afine operation randomly
                    do_rotation = random.choice([False, True])
                    print("- rotate:", do_rotation)
                    do_scale = random.choice([False, False, True])
                    print("- scale:", do_scale)
                    do_translation = random.choice([False, False, False, True])
                    print("- translate:", do_translation)

                    # cv2.imshow("pokues seg pred transf", seg_slice)

                    if do_rotation:
                        angle = random.randint(-27, 27)
                        slice = rotate(img=slice, angle=angle)
                        preprocessed_slice = rotate(img=preprocessed_slice, angle=angle)
                        seg_slice = rotate(img=seg_slice, angle=angle)

                    if do_scale:
                        scale_val = random.uniform(0.6, 1.0)
                        slice = scale(img=slice, scale_val=scale_val)
                        preprocessed_slice = scale(img=preprocessed_slice, scale_val=scale_val)
                        seg_slice = scale(img=seg_slice, scale_val=scale_val)

                    if do_translation:
                        x = random.randint(-15, -15)
                        y = random.randint(-10, 10)

                        slice = translate(img=slice, x=x, y=y)
                        preprocessed_slice = translate(img=preprocessed_slice, x=x, y=y)
                        seg_slice = translate(img=seg_slice, x=x, y=y)

                    if not do_rotation and not do_scale and not do_translation:
                        angle = random.randint(-27, 27)
                        slice = rotate(img=slice, angle=angle)
                        preprocessed_slice = rotate(img=preprocessed_slice, angle=angle)
                        seg_slice = rotate(img=seg_slice, angle=angle)

                    if np.amin(slice) < 0:
                        print("- pocet odstranenych zapornych hodnot: ", len(slice[np.where(slice < 0)]))
                        slice[np.where(slice < 0)] = 0

                    # save results to dataset
                    numpngw.write_png(patient_exam_folder + '/2D/axial_original_'+str(index)+'.png', np.uint16(original))
                    numpngw.write_png(patient_exam_folder + '/2D/axial_original_transformed_'+str(index)+'.png', np.uint16(slice))
                    numpngw.write_png(patient_exam_folder + '/2D/axial_processed_'+str(index)+'.png', np.uint16(original_preprocessed))
                    numpngw.write_png(patient_exam_folder + '/2D/axial_processed_transformed_'+str(index)+'.png', np.uint16(preprocessed_slice))
                    numpngw.write_png(patient_exam_folder + '/2D/axial_seg_'+str(index)+'.png', np.uint16(original_seg))
                    numpngw.write_png(patient_exam_folder + '/2D/axial_seg_transformed_'+str(index)+'.png', np.uint16(seg_slice))

                    # pokus = cv2.imread(path_to_patient + '/' + patient_exam_folder + '/2D/axial_original_'+str(index)+'.png', -1)
                    # cv2.imshow("original", pokus)
                    # pokus = cv2.imread('axial_original_transformed_'+str(index)+'.png', -1)
                    # cv2.imshow("orginal transformed", pokus)
                    # pokus = cv2.imread('axial_processed_' + str(index) + '.png', -1)
                    # cv2.imshow("processed", pokus)
                    # pokus = cv2.imread('axial_processed_transformed'+str(index)+'.png', -1)
                    # cv2.imshow("processed transformed", pokus)
                    # pokus = cv2.imread('axial_seg_' + str(index) + '.png', -1)
                    # cv2.imshow("original seg", pokus)
                    # pokus = cv2.imread('axial_seg_transformed_' + str(index) + '.png', -1)
                    # cv2.imshow("transformed seg", pokus)

                    # cv2.waitKey()

                else:
                    print("- nepouzivam")

                print("--------------------------------------------")
        else:
            print("Zly pocet slicov v segmentacii a volume", examination.name)

