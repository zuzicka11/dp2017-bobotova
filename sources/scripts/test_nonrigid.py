import os
import pandas as pd

from logic.reader import Reader
from logic.preprocessing import Preprocessing
from logic.nonrigid import NonRigidRegistration
from scripts.compare_images import compare_images

# test nonrigid registration

tests = [
    {
        'optical_flow_type': 'Farneback',
        'modality': 'flair',
        'foldeer': 'farneback_flair',
        'csv_name': 'farneback_flair.csv'
    },
    # {
    #     'optical_flow_type': 'Farneback',
    #     'modality': 't1',
    #     'foldeer': 'farneback_t1',
    #     'csv_name': 'farneback_t1.csv'
    # },
    # {
    #     'optical_flow_type': 'Farneback',
    #     'modality': 't1c',
    #     'foldeer': 'farneback_t1c',
    #     'csv_name': 'farneback_t1c.csv'
    # },
    # {
    #     'optical_flow_type': 'Farneback',
    #     'modality': 't2',
    #     'foldeer': 'farneback_t2',
    #     'csv_name': 'farneback_t2.csv'
    # },
    # {
    #     'optical_flow_type': 'LK',
    #     'modality': 'flair',
    #     'foldeer': 'lk_flair',
    #     'csv_name': 'lk_flair.csv'
    # },
    # {
    #     'optical_flow_type': 'LK',
    #     'modality': 't1',
    #     'foldeer': 'lk_t1',
    #     'csv_name': 'lk_t1.csv'
    # },
    # {
    #     'optical_flow_type': 'LK',
    #     'modality': 't1c',
    #     'foldeer': 'lk_t1c',
    #     'csv_name': 'lk_t1c.csv'
    # },
    # {
    #     'optical_flow_type': 'LK',
    #     'modality': 't2',
    #     'foldeer': 'lk_t2',
    #     'csv_name': 'lk_t2.csv'
    # }
]
paths_to_patients = [
    "../../data/nonrigid_dataset/pat_153",
    "../../data/nonrigid_dataset/pat_171",
    "../../data/nonrigid_dataset/pat_198",
    "../../data/nonrigid_dataset/pat_222",
    "../../data/nonrigid_dataset/pat_226",
    "../../data/nonrigid_dataset/pat_230",
    "../../data/nonrigid_dataset/pat_260",
    "../../data/nonrigid_dataset/pat_280",
    "../../data/nonrigid_dataset/pat_290",
    "../../data/nonrigid_dataset/pat_309",
    "../../data/nonrigid_dataset/pat_314",
    "../../data/nonrigid_dataset/pat_370",
    "../../data/nonrigid_dataset/pat_374",
    "../../data/nonrigid_dataset/pat_377",
    "../../data/nonrigid_dataset/pat_396",
    "../../data/nonrigid_dataset/pat_399",
    "../../data/nonrigid_dataset/pat_417",
    "../../data/nonrigid_dataset/pat_439",
    "../../data/nonrigid_dataset/pat_444",
    "../../data/nonrigid_dataset/pat_447",
]

for test in tests:

    print(test)

    # create dataframe to store results
    df = pd.DataFrame(columns=['name', 'correlation_an', 'intersection_an', 'bhattacharyya_an', 'correlation_wn', 'intersection_wn','bhattacharyya_wn'])

    # for whole dataset
    for path in paths_to_patients:

        # LOAD DATA TO EXAMINATION OBJECT
        patient_examinations_paths = [f.path for f in os.scandir(path) if f.is_dir()]
        reader = Reader()

        print(patient_examinations_paths)

        patient_examinations = []
        for path in patient_examinations_paths:
            patient_examinations.append(reader.read(patient_examination_path=path))

        # sort examinations
        patient_examinations.sort(key=lambda x: x.name, reverse=False)

        # PREPROCESSING - depends on tsetd modality - every modality is tested independently
        print("Template:", patient_examinations[0].name)
        if test['modality'] == 'flair':
            preprocessing = Preprocessing(template=patient_examinations[0].mri_flair.data, ignore_zero_values=True, remove_percentile=True, save_results=False, path=patient_examinations[0].path+"/"+test['foldeer'])
            patient_examinations[0].mri_flair.data = preprocessing.get_processed_teplate()
            for idx in range(1, len(patient_examinations)):
                print(" - next:", patient_examinations[idx].name)
                patient_examinations[idx].mri_flair.data = preprocessing.process_data(patient_examinations[idx].mri_flair.data, path=patient_examinations[idx].path+"/"+test['foldeer'])

        if test['modality'] == 't1':
            preprocessing = Preprocessing(template=patient_examinations[0].mri_t1.data, ignore_zero_values=True, remove_percentile=True, save_results=False, path=patient_examinations[0].path+"/"+test['foldeer'])
            patient_examinations[0].mri_t1.data = preprocessing.get_processed_teplate()
            for idx in range(1, len(patient_examinations)):
                print(" - next:", patient_examinations[idx].name)
                patient_examinations[idx].mri_t1.data = preprocessing.process_data(patient_examinations[idx].mri_t1.data, path=patient_examinations[idx].path+"/"+test['foldeer'])

        if test['modality'] == 't1c':
            preprocessing = Preprocessing(template=patient_examinations[0].mri_t1c.data, ignore_zero_values=True, remove_percentile=True, save_results=False, path=patient_examinations[0].path+"/"+test['foldeer'])
            patient_examinations[0].mri_t1c.data = preprocessing.get_processed_teplate()
            for idx in range(1, len(patient_examinations)):
                print(" - next:", patient_examinations[idx].name)
                patient_examinations[idx].mri_t1c.data = preprocessing.process_data(patient_examinations[idx].mri_t1c.data, path=patient_examinations[idx].path+"/"+test['foldeer'])

        if test['modality'] == 't2':
            preprocessing = Preprocessing(template=patient_examinations[0].mri_t2.data, ignore_zero_values=True, remove_percentile=True, save_results=False, path=patient_examinations[0].path+"/"+test['foldeer'])
            patient_examinations[0].mri_t2.data = preprocessing.get_processed_teplate()
            for idx in range(1, len(patient_examinations)):
                print(" - next:", patient_examinations[idx].name)
                patient_examinations[idx].mri_t2.data = preprocessing.process_data(patient_examinations[idx].mri_t2.data, path=patient_examinations[idx].path+"/"+test['foldeer'])

        # register examinations non-rigidly
        nrr = NonRigidRegistration(registration_type=test['optical_flow_type'], save_results=True)
        registered_examinations = nrr.process_volumes(patient_examinations, modality=test['modality'])

        # compare inputs and outputs
        for idx, examination in enumerate(registered_examinations[:-1]):

            warped = examination.warped_data.data

            if test['modality'] == 'flair':
                actual = examination.mri_flair.data
                future = registered_examinations[idx+1].mri_flair.data
            if test['modality'] == 't1':
                actual = examination.mri_t1.data
                future = registered_examinations[idx + 1].mri_t1.data
            if test['modality'] == 't1c':
                actual = examination.mri_t1c.data
                future = registered_examinations[idx + 1].mri_t1c.data
            if test['modality'] == 't2':
                actual = examination.mri_t2.data
                future = registered_examinations[idx + 1].mri_t2.data

            print(actual.shape, actual.dtype)
            print(warped.shape, warped.dtype)
            print(future.shape, future.dtype)

            i = 0
            for f_a, f_w, f_f in zip(actual, warped, future):

                p = examination.path + "/"+test['foldeer']+"/" + str(i)

                if os.path.exists(p):
                    # compare old input with actual input
                    correlation1, intersection1, bhattacharyya1 = compare_images(img_1=f_a, img_2=f_f, path=p, diff='a+n', save_results=False)
                    # compare old but warped input with actual input
                    correlation2, intersection2, bhattacharyya2 = compare_images(img_1=f_w, img_2=f_f, path=p, diff='w+n', save_results=False)
                    name = examination.name + '_' + registered_examinations[idx+1].name + '_' + str(i)
                    print(name)
                    print(correlation1, intersection1, bhattacharyya1)
                    print(correlation2, intersection2, bhattacharyya2)

                    # save metrics to dataframe
                    df = df.append(
                        pd.Series(data={
                            'name': name,
                            'correlation_an': correlation1,
                            'intersection_an': intersection1,
                            'bhattacharyya_an': bhattacharyya1,
                            'correlation_wn': correlation2,
                            'intersection_wn': intersection2,
                            'bhattacharyya_wn': bhattacharyya2
                        }), ignore_index=True
                    )

                i += 1
    # save dataframe
    df.to_csv(test['csv_name'])




