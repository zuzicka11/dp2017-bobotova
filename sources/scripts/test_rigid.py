import os
import time
import cv2
import pandas as pd
from logic.rigid import RigidRegistration
from scripts.compare_images import compare_images

# test rigid registration

# find specific file
def find_file(file_name, path_for_searching):

    resulting_file = None

    for (path, dirs, files) in os.walk(path_for_searching):
        for file in files:
            if file_name == file:
                resulting_file = os.path.join(path, file)
                return resulting_file

    return resulting_file

tests = [
    # {
    #     'folder': '/results_health_brain_sift_sift',
    #     'registration_mask_type': 'health_brain',
    #     'key_point_detector': 'SIFT',
    #     'key_point_descriptor': 'SIFT',
    #     'csv_name': 'health_brain_SIFT_SIFTF.csv'
    #
    # },
    # {
    #     'folder': '/results_health_brain_surf_surf',
    #     'registration_mask_type': 'health_brain',
    #     'key_point_detector': 'SURF',
    #     'key_point_descriptor': 'SURF',
    #     'csv_name': 'health_brain_SURF_SURF.csv'
    #
    # },
    # {
    #     'folder': '/results_health_brain_orb_orb',
    #     'registration_mask_type': 'health_brain',
    #     'key_point_detector': 'ORB',
    #     'key_point_descriptor': 'ORB',
    #     'csv_name': 'health_brain_ORB_ORB.csv'
    #
    # },
    # {
    #     'folder': '/results_health_brain_fast_sift',
    #     'registration_mask_type': 'health_brain',
    #     'key_point_detector': 'FAST',
    #     'key_point_descriptor': 'SIFT',
    #     'csv_name': 'health_brain_FAST_SIFT.csv'
    #
    # },
    # {
    #     'folder': '/results_health_brain_fast_surf',
    #     'registration_mask_type': 'health_brain',
    #     'key_point_detector': 'FAST',
    #     'key_point_descriptor': 'SURF',
    #     'csv_name': 'health_brain_FAST_SURF.csv'
    #
    # },
    {
        'folder': '/results_cranium_sift_sift',
        'registration_mask_type': 'cranium',
        'key_point_detector': 'SIFT',
        'key_point_descriptor': 'SIFT',
        'csv_name': 'cranium_SIFT_SIFT.csv'

    },
    {
        'folder': '/results_cranium_surf_surf',
        'registration_mask_type': 'cranium',
        'key_point_detector': 'SURF',
        'key_point_descriptor': 'SURF',
        'csv_name': 'cranium_SURF_SURF.csv'

    },
{
        'folder': '/results_cranium_orb_orb',
        'registration_mask_type': 'cranium',
        'key_point_detector': 'ORB',
        'key_point_descriptor': 'ORB',
        'csv_name': 'cranium_ORB_ORB.csv'

    },
    {
        'folder': '/results_cranium_fast_sift',
        'registration_mask_type': 'cranium',
        'key_point_detector': 'FAST',
        'key_point_descriptor': 'SIFT',
        'csv_name': 'cranium_FAST_SIFT.csv'

    },
    {
        'folder': '/results_cranium_fast_surf',
        'registration_mask_type': 'cranium',
        'key_point_detector': 'FAST',
        'key_point_descriptor': 'SURF',
        'csv_name': 'cranium_FAST_SURF.csv'

    },
    ]
paths_to_patients = [
    "../../data/rigid_dataset/subj_1",
    "../../data/rigid_dataset/subj_3",
    "../../data/rigid_dataset/subj_6",
    "../../data/rigid_dataset/subj_9",
    "../../data/rigid_dataset/subj_10",
    "../../data/rigid_dataset/subj_12",
    "../../data/rigid_dataset/subj_13",
    "../../data/rigid_dataset/subj_16",
    "../../data/rigid_dataset/subj_19",
    "../../data/rigid_dataset/subj_20",
    "../../data/rigid_dataset/subj_21",
    "../../data/rigid_dataset/subj_25",
    "../../data/rigid_dataset/subj_26",
    "../../data/rigid_dataset/subj_27",
    "../../data/rigid_dataset/subj_30",
    "../../data/rigid_dataset/subj_32",
    "../../data/rigid_dataset/subj_33",
    "../../data/rigid_dataset/subj_34",
    "../../data/rigid_dataset/subj_36",
    "../../data/rigid_dataset/subj_38",
    "../../data/rigid_dataset/subj_40",
    "../../data/rigid_dataset/subj_41",
    "../../data/rigid_dataset/subj_42",
    "../../data/rigid_dataset/subj_45",
    "../../data/rigid_dataset/subj_46",
    "../../data/rigid_dataset/subj_48",
    "../../data/rigid_dataset/subj_49",
    "../../data/rigid_dataset/subj_50",
    "../../data/rigid_dataset/subj_55",
    "../../data/rigid_dataset/subj_56",
    "../../data/rigid_dataset/subj_58",
    "../../data/rigid_dataset/subj_59",
    "../../data/rigid_dataset/subj_62",
    "../../data/rigid_dataset/subj_66",
    "../../data/rigid_dataset/subj_68",
    "../../data/rigid_dataset/subj_71",
    "../../data/rigid_dataset/subj_73",
    "../../data/rigid_dataset/subj_74",
    "../../data/rigid_dataset/subj_75",
    "../../data/rigid_dataset/subj_82",
    "../../data/rigid_dataset/subj_83",
    "../../data/rigid_dataset/subj_84",
    "../../data/rigid_dataset/subj_86",
    "../../data/rigid_dataset/subj_89",
    "../../data/rigid_dataset/subj_92",
    "../../data/rigid_dataset/subj_93",
    "../../data/rigid_dataset/subj_95",
    "../../data/rigid_dataset/subj_100",
    "../../data/rigid_dataset/subj_107",
    "../../data/rigid_dataset/subj_108",
    "../../data/rigid_dataset/subj_114",
    "../../data/rigid_dataset/subj_119",
    "../../data/rigid_dataset/subj_120",
    "../../data/rigid_dataset/subj_126",
    "../../data/rigid_dataset/subj_127",
    "../../data/rigid_dataset/subj_130",
    "../../data/rigid_dataset/subj_131",
    "../../data/rigid_dataset/subj_134",
    "../../data/rigid_dataset/subj_136",
    "../../data/rigid_dataset/subj_137",
    "../../data/rigid_dataset/subj_142",
    "../../data/rigid_dataset/subj_143",
    "../../data/rigid_dataset/subj_146",
    "../../data/rigid_dataset/subj_148",
    "../../data/rigid_dataset/subj_149",
    "../../data/rigid_dataset/subj_151",
    "../../data/rigid_dataset/subj_155",
    "../../data/rigid_dataset/subj_158",
    "../../data/rigid_dataset/subj_159",
    "../../data/rigid_dataset/subj_161",
    "../../data/rigid_dataset/subj_165"
]

for test in tests:

    # create dataframe to store results
    df = pd.DataFrame(columns=['name', 'count_keypoints_img_1', 'count_keypoints_img_2', 'count_all_matches', 'count_good_matches', 'matrix_len', 'correlation_ri', 'intersection_ri', 'bhattacharyya_ri', 'correlation_rw', 'intersection_rw', 'bhattacharyya_rw', 'correlation_oi', 'intersection_oi', 'bhattacharyya_oi', 'correlation_ow', 'intersection_ow', 'bhattacharyya_ow', 'execution_time'])

    # initialize rigid registration
    rr = RigidRegistration(registration_mask_type=test['registration_mask_type'], key_point_detector=test['key_point_detector'], key_point_descriptor=test['key_point_descriptor'], save_results=True)
    save_results = True

    # for whole dataset
    for idx, path in enumerate(paths_to_patients):

        if idx == 10:
            rr = RigidRegistration(registration_mask_type=['registration_mask_type'], key_point_detector=test['key_point_detector'], key_point_descriptor=test['key_point_descriptor'], save_results=False)
            save_results = False

        patient_examinations_paths = [f.path for f in os.scandir(path) if f.is_dir()]
        patient_examinations_paths.sort(key=lambda x: x, reverse=False)
        patient_examinations_paths = [s + "/2D" for s in patient_examinations_paths]

        print(path)

        if len(patient_examinations_paths) > 1:

            # get referencing image
            referencing_examination_path = patient_examinations_paths[0]

            # register rigidly aditional images to the referencing image
            for index in range(0, 300):
                print(index)

                reference_image_path = find_file("axial_processed_" + str(index) + ".png", referencing_examination_path)
                reference_segmentation_path = find_file("axial_seg_" + str(index) + ".png", referencing_examination_path)

                if reference_image_path is not None and reference_segmentation_path is not None:

                    print("Idem registrovat: ")
                    print(" - reference:", reference_image_path, reference_segmentation_path)

                    reference_image16 = cv2.imread(reference_image_path, -1)
                    reference_image = (reference_image16 / 256).astype('uint8')
                    reference_segmentation16 = cv2.imread(reference_segmentation_path, -1)
                    reference_segmentation = (reference_segmentation16 / 256).astype('uint8')
                    # cv2.imshow("Reference image", reference_image)
                    # cv2.imshow("Reference segmentation", reference_segmentation)

                    for actual_examination_path in patient_examinations_paths[1:]:

                        actual_image_path = find_file("axial_processed_transformed" + str(index) + ".png", actual_examination_path)
                        actual_image_for_comparision_path = find_file("axial_processed_" + str(index) + ".png", actual_examination_path)
                        actual_segmentation_path = find_file("axial_seg_transformed_" + str(index) + ".png", actual_examination_path)

                        if actual_image_path is not None and actual_segmentation_path is not None:

                            print(" -    actual:", actual_image_path, actual_segmentation_path)

                            my_name = os.path.basename(os.path.dirname(referencing_examination_path)) + "_" + os.path.basename(os.path.dirname(actual_examination_path)) + "_" + str(index)

                            if save_results:
                                save_path = os.path.dirname(actual_image_path)
                                save_path += test['folder']
                                if not os.path.exists(save_path):
                                    os.makedirs(save_path)
                                save_path += "/" + os.path.basename(os.path.dirname(os.path.dirname(actual_examination_path))) + "_" + str(index)
                                if not os.path.exists(save_path):
                                    os.makedirs(save_path)

                            actual_image16 = cv2.imread(actual_image_path, -1)
                            actual_image = (actual_image16 / 256).astype('uint8')
                            actual_image_for_comparision16 = cv2.imread(actual_image_for_comparision_path, -1)
                            actual_image_for_comparision = (actual_image_for_comparision16 / 256).astype('uint8')
                            actual_segmentation16 = cv2.imread(actual_segmentation_path, -1)
                            actual_segmentation = (actual_segmentation16 / 256).astype('uint8')
                            # cv2.imshow("Actual image", actual_image)
                            # cv2.imshow("Actual segmentation", actual_segmentation)

                            start_time = time.time()
                            # register data
                            wrapped, data = rr.rigid_registration(img_1=reference_image.copy(), img_2=actual_image.copy(), img_compare=actual_image_for_comparision.copy(), mask_1=reference_segmentation, mask_2=actual_segmentation, path=save_path)
                            end_time = time.time() - start_time

                            # compare inputs and outputs
                            correlation_ri, intersection_ri, bhattacharyya_ri = [0, 0, 0]
                            correlation_rw, intersection_rw, bhattacharyya_rw = [0, 0, 0]
                            correlation_oi, intersection_oi, bhattacharyya_oi = [0, 0, 0]
                            correlation_ow, intersection_ow, bhattacharyya_ow = [0, 0, 0]

                            if wrapped is not None:
                                cv2.imshow("wrapped", wrapped)
                                cv2.waitKey(100)
                                # compare reference_image and actual_image
                                correlation_ri, intersection_ri, bhattacharyya_ri = compare_images(img_1=reference_image, img_2=actual_image, path=save_path, diff='ri', save_results=False)
                                # compare reference_image and actual_image after warping
                                correlation_rw, intersection_rw, bhattacharyya_rw = compare_images(img_1=reference_image, img_2=wrapped, path=save_path, diff='rw', save_results=False)
                                # compare originaly registered actual_image with actual_image from the dataset
                                correlation_oi, intersection_oi, bhattacharyya_oi = compare_images(img_1=actual_image_for_comparision, img_2=actual_image, path=save_path, diff='oi', save_results=False)
                                # compare originaly registered actual_image with actual_image after warping
                                correlation_ow, intersection_ow, bhattacharyya_ow = compare_images(img_1=actual_image_for_comparision, img_2=wrapped, path=save_path, diff='ow', save_results=False)

                            # print(" - seconds: ",  end_time)
                            # print(data)
                            # print(correlation_rw, intersection_rw, bhattacharyya_rw)
                            # print(correlation_ri, intersection_ri, bhattacharyya_ri)
                            # print(correlation_ow, intersection_ow, bhattacharyya_ow)
                            # print(correlation_oi, intersection_oi, bhattacharyya_oi)

                            # save metrics to dataframe
                            df = df.append(
                                    pd.Series(data={
                                        'name': my_name,
                                        'count_keypoints_img_1': data['count_keypoints_img_1'],
                                        'count_keypoints_img_2': data['count_keypoints_img_2'],
                                        'count_all_matches': data['count_all_matches'],
                                        'count_good_matches': data['count_good_matches'],
                                        'matrix_len': data['matrix_len'],
                                        'correlation_ri': correlation_ri,
                                        'intersection_ri': intersection_ri,
                                        'bhattacharyya_ri': bhattacharyya_ri,
                                        'correlation_rw': correlation_rw,
                                        'intersection_rw': intersection_rw,
                                        'bhattacharyya_rw': bhattacharyya_rw,
                                        'correlation_oi': correlation_oi,
                                        'intersection_oi': intersection_oi,
                                        'bhattacharyya_oi': bhattacharyya_oi,
                                        'correlation_ow': correlation_ow,
                                        'intersection_ow': intersection_ow,
                                        'bhattacharyya_ow': bhattacharyya_ow,
                                        'execution_time': end_time
                                    }), ignore_index=True
                                )

                print("-----------------------------------------------")

        else:
            print("Not enough examination: ", len(patient_examinations_paths))

    # save dataframe
    df.to_csv(test['csv_name'])
