# examination object
class Examination:

    def __init__(self):
        self.path = None            # path to the patient examination
        self.name = None            # name of the examination
        self.mri_t1 = None          # t1 Volume
        self.mri_t1c = None         # t1C Volume
        self.mri_t2 = None          # t2 Volume
        self.mri_flair = None       # flair Volume
        self.segmentation = None    # segmentation Volume

        self.warped_data = None     # space for warped Volume data
        self.own_points = None      # points of current examination
        self.next_points = None     # corresponding point of the next examination
