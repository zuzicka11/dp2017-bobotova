import SimpleITK as sitk


# Volume object
class Volume:
    def __init__(self, sitk_object): # using simpleITK to create Volume object and reach important data
        self.origin = sitk_object.GetOrigin()
        self.size = sitk_object.GetSize()
        self.spacing = sitk_object.GetSpacing()
        self.direction = sitk_object.GetDirection()
        self.data = sitk.GetArrayFromImage(sitk_object).astype('int16')  # volumetric data in numpy array
