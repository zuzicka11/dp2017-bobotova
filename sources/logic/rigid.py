import cv2
import numpy as np

class RigidRegistration:

    def __init__(self, registration_mask_type='health_brain', key_point_detector='SIFT', key_point_descriptor='SIFT', show_results=False, save_results=False):
        self.registration_mask_type = registration_mask_type  # 'cranium' # 'health_brain'
        self.key_point_detector = key_point_detector
        self.key_point_descriptor = key_point_descriptor
        self.show_results = show_results
        self.save_results = save_results

    # method for rigid registration of two input slices
    def rigid_registration(self, img_1, img_2, img_compare, mask_1, mask_2, path=None):

        ## Step 1: create mask for registration
        img_1_registrationMask = self.__create_mask_for_registration(image=img_1, segmentation_mask=mask_1, idx='1', path=path)
        img_2_registrationMask = self.__create_mask_for_registration(image=img_2, segmentation_mask=mask_2, idx='2', path=path)

        ## Step 2: Detect the keypoints using SIFT, SURF, ORB or FAST Detector
        keypoints_1 = []
        keypoints_2 = []
        all_matches = []
        good_matches = []
        afine = []
        img_result = None

        if self.key_point_detector == 'SIFT':
            keypoints_1 = self.__detect_keypoints_SIFT(image=img_1, registration_mask=img_1_registrationMask, idx='1', path=path)
            keypoints_2 = self.__detect_keypoints_SIFT(image=img_2, registration_mask=img_2_registrationMask, idx='2', path=path)
        elif self.key_point_detector == 'SURF':
            keypoints_1 = self.__detect_keypoints_SURF(image=img_1, registration_mask=img_1_registrationMask, idx='1', path=path)
            keypoints_2 = self.__detect_keypoints_SURF(image=img_2, registration_mask=img_2_registrationMask, idx='2', path=path)
        elif self.key_point_detector == 'FAST':
            keypoints_1 = self.__detect_keypoints_FAST(image=img_1, registration_mask=img_1_registrationMask, idx='1', path=path)
            keypoints_2 = self.__detect_keypoints_FAST(image=img_2, registration_mask=img_2_registrationMask, idx='2', path=path)
        elif self.key_point_detector == 'ORB':
            keypoints_1 = self.__detect_keypoints_ORB(image=img_1, registration_mask=img_1_registrationMask, idx='1', path=path)
            keypoints_2 = self.__detect_keypoints_ORB(image=img_2, registration_mask=img_2_registrationMask, idx='2', path=path)
        else:
            print("Unknown keypoint detector!")

        if len(keypoints_1) and len(keypoints_2):
            ## Step 3: Calculate descriptors (feature vectors)
            descriptors_1 = []
            descriptors_2 = []

            if self.key_point_descriptor == 'SIFT':
                descriptors_1 = self.__calculate_descriptors_SIFT(image=img_1, keypoints=keypoints_1)
                descriptors_2 = self.__calculate_descriptors_SIFT(image=img_2, keypoints=keypoints_2)
            elif self.key_point_descriptor == 'SURF':
                descriptors_1 = self.__calculate_descriptors_SURF(image=img_1, keypoints=keypoints_1)
                descriptors_2 = self.__calculate_descriptors_SURF(image=img_2, keypoints=keypoints_2)
            elif self.key_point_descriptor == 'ORB':
                descriptors_1 = self.__calculate_descriptors_ORB(image=img_1, keypoints=keypoints_1)
                descriptors_2 = self.__calculate_descriptors_ORB(image=img_2, keypoints=keypoints_2)

            ## Step 4: Matching descriptor vectors with a brute force matcher
            all_matches = self.__get_matches(desc_1=descriptors_1, desc_2=descriptors_2, img_1=img_1, img_2=img_2, keypoints_1=keypoints_1, keypoints_2=keypoints_2, path=path)

            ## Step 5: Filter only "good" matches
            if self.registration_mask_type == 'health_brain':
                good_matches = self.__get_good_matches(matches=all_matches, keypoints_1=keypoints_1, keypoints_2=keypoints_2, img_1=img_1, img_2=img_2, path=path)
            elif self.registration_mask_type == 'cranium':
                good_matches = self.__get_good_matches(matches=all_matches, keypoints_1=keypoints_1, keypoints_2=keypoints_2, img_1=img_1, img_2=img_2, path=path, tolerancy_lr=20, tolerancy_tb=20)

            if good_matches is not None and len(good_matches) > 3:
                ## Step 6: Rigid transformation of input image 1 to input image 2
                src_pts = np.float32([keypoints_1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
                dst_pts = np.float32([keypoints_2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)

                # (homography, mask) = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
                # find afine transform
                afine = cv2.estimateRigidTransform(dst_pts, src_pts, False)

                if afine is not None:
                    h, w = img_2.shape
                    # img_result = cv2.warpPerspective(img_2, afine, (img_2.shape[1], img_2.shape[0]))
                    # apply afine transform
                    img_result = cv2.warpAffine(img_2, afine, (img_2.shape[1], img_2.shape[0]))

                    if self.show_results:
                        cv2.imshow('Wrap perspective ', img_result)

                    if self.save_results:
                        cv2.imwrite(path + '/04_wrap_perspective.png', img_result)

                    # paint input and ouput images in one image using color chanels to see success of the method
                    if self.show_results or self.save_results:
                        img_1 = img_1[0:258, 0:222]
                        img1_rgb = cv2.cvtColor(img_1, cv2.COLOR_GRAY2BGR)
                        img1_rgb[:, :, 0] = 0  # set blue channels to 0

                        img_2 = img_2[0:258, 0:222]
                        img2_rgb = cv2.cvtColor(img_2, cv2.COLOR_GRAY2BGR)
                        img2_rgb[:, :, 1] = 0  # set green channels to 0
                        img2_rgb[:, :, 2] = 0  # set red channels to 0

                        img_result = img_result[0:258, 0:222]
                        img_result_rgb = cv2.cvtColor(img_result, cv2.COLOR_GRAY2BGR)
                        img_result_rgb[:, :, 1] = 0  # set green channels to 0
                        img_result_rgb[:, :, 2] = 0  # set red channels to 0

                        img_compare = img_compare[0:258, 0:222]
                        img_compare_rgb = cv2.cvtColor(img_compare, cv2.COLOR_GRAY2BGR)
                        img_compare_rgb[:, :, 0] = 0  # set blue channels to 0

                        input = img1_rgb + img2_rgb
                        output = img1_rgb + img_result_rgb

                        input_and_copare = img_compare_rgb + img2_rgb
                        output_and_copare = img_compare_rgb + img_result_rgb

                        if self.show_results:
                            cv2.imshow('Input images', input)
                            cv2.imshow('Output images', output)
                            cv2.imshow('Input with original', input_and_copare)
                            cv2.imshow('Output with original', output_and_copare)

                        if self.save_results:
                            cv2.imwrite(path + '/05a_inputs_alignment.png', input)
                            cv2.imwrite(path + '/05b_outputs_alignment.png', output)
                            cv2.imwrite(path + '/05c_input_original_alignment.png', input_and_copare)
                            cv2.imwrite(path + '/05d_output_original_alignment.png', output_and_copare)
                else:
                    afine = []

        if self.show_results:
            cv2.waitKey()

        return img_result, {'count_keypoints_img_1': len(keypoints_1),
                            'count_keypoints_img_2': len(keypoints_2),
                            'count_all_matches': len(all_matches),
                            'count_good_matches': len(good_matches),
                            'matrix_len': len(afine),
                            }

    # creation of mask for area which we will work with in registration process
    def __create_mask_for_registration(self, image, segmentation_mask, idx, path):

        # threshold input image first to get mask of brain
        _, thresholded_image = cv2.threshold(image, 10, 255, cv2.THRESH_BINARY)

        if self.save_results:
            cv2.imwrite(path + '/01a_mask_for_registration'+idx+'.png', thresholded_image)

        result = np.zeros_like(image)
        result[:] = 255

        if self.registration_mask_type is 'health_brain':   # find parts of healt brain
            # reduction of mask to remove margins of brain
            kernel = np.ones((5, 5), np.uint8)
            thresholded_image = cv2.dilate(thresholded_image, kernel=kernel, iterations=1)
            kernel = np.ones((15, 15), np.uint8)
            thresholded_image = cv2.erode(thresholded_image, kernel=kernel, iterations=1)

            if self.save_results:
                cv2.imwrite(path + '/01b_mask_for_registration' + idx + '.png', thresholded_image)

            # substract tumor from mask - registration will not work on tumor part too
            result = thresholded_image - segmentation_mask
            _, result = cv2.threshold(result, 250, 255, cv2.THRESH_BINARY)

        elif self.registration_mask_type is 'cranium': # fiand cranium in the image
            kernel = np.ones((10, 10), np.uint8)
            thresholded_image = cv2.dilate(thresholded_image, kernel=kernel, iterations=1)
            kernel = np.ones((7, 7), np.uint8)
            thresholded_image = cv2.erode(thresholded_image, kernel=kernel, iterations=1)
            _, contours, _ = cv2.findContours(thresholded_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            result = np.zeros_like(image)
            cv2.drawContours(result, contours, -1, 255, 20)

        if self.show_results:
            cv2.imshow('Mask for registration '+idx, result)

        if self.save_results:
            cv2.imwrite(path + '/01c_mask_for_registration'+idx+'.png', result)

        return result

    # function for painting keypoints
    def __panit_keypoints(self, image, keypoints, idx, path):
        img = cv2.drawKeypoints(image, keypoints, None, color=(0, 0, 255), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        if self.show_results:
            cv2.imshow(self.key_point_detector+' '+idx, img)

        if self.save_results:
            cv2.imwrite(path + '/02_detect_keypoints'+self.key_point_detector+'_'+idx+'.png', img)

    # detect keypoint using SIFT detector
    def __detect_keypoints_SIFT(self, image, registration_mask, idx, path):
        sift = cv2.xfeatures2d.SIFT_create()
        keypoints = sift.detect(image, registration_mask)

        if self.show_results or self.save_results:
            self.__panit_keypoints(image, keypoints, idx, path)

        return keypoints

    # detect keypoint using SURF detector
    def __detect_keypoints_SURF(self, image, registration_mask, idx, path):
        surf = cv2.xfeatures2d.SURF_create(400)
        keypoints = surf.detect(image, registration_mask)

        if self.show_results or self.save_results:
            self.__panit_keypoints(image, keypoints, idx, path)

        return keypoints

    # detect keypoint using ORB detector
    def __detect_keypoints_ORB(self, image, registration_mask, idx, path):
        orb = cv2.ORB_create()
        keypoints = orb.detect(image, registration_mask)

        if self.show_results or self.save_results:
            self.__panit_keypoints(image, keypoints, idx, path)

        return keypoints

    # detect keypoint using FAST detector
    def __detect_keypoints_FAST(self, image, registration_mask, idx, path):
        fast = cv2.FastFeatureDetector_create()
        keypoints = fast.detect(image, registration_mask)

        if self.show_results or self.save_results:
            self.__panit_keypoints(image, keypoints, idx, path)

        return keypoints

    # compute descriptors using SIFT
    def __calculate_descriptors_SIFT(self, image, keypoints):
        sift = cv2.xfeatures2d.SIFT_create()
        _, descriptors = sift.compute(image, keypoints)

        return descriptors

    # compute descriptors using SURF
    def __calculate_descriptors_SURF(self, image, keypoints):
        surf = cv2.xfeatures2d.SURF_create(400)
        _, descriptors = surf.compute(image, keypoints)

        return descriptors

    # compute descriptors using ORB
    def __calculate_descriptors_ORB(self, image, keypoints):
        orb = cv2.ORB_create()
        _, descriptors = orb.compute(image, keypoints)

        return descriptors

    # get all matches (depending on computed descriptors) between the keypoints of both images
    def __get_matches(self, desc_1, desc_2, img_1, img_2, keypoints_1, keypoints_2, path):
        # create BFMatcher object
        if self.key_point_descriptor == 'SIFT' or self.key_point_descriptor == 'SURF':
            bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
        else:
            bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

        # match descriptors
        matches = bf.match(desc_1, desc_2)

        if self.show_results:
            img_matches_all = cv2.drawMatches(img_1, keypoints_1, img_2, keypoints_2, matches, None, matchColor=(0, 0, 255), singlePointColor=(0, 0, 255), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
            cv2.imshow('All matches', img_matches_all)

        if self.save_results:
            img_matches_all = cv2.drawMatches(img_1, keypoints_1, img_2, keypoints_2, matches, None, matchColor=(0, 0, 255), singlePointColor=(0, 0, 255), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
            cv2.imwrite(path + '/02_all_matches.png', img_matches_all)

        return matches

    # filter good matches from all matches - they should not be very far away
    def __get_good_matches(self, matches, keypoints_1, keypoints_2, img_1, img_2, path, tolerancy_lr=20, tolerancy_tb=20):

        good_matches = []
        for i in range(0, len(matches)):

            x1 = keypoints_1[matches[i].queryIdx].pt[0]
            y1 = keypoints_1[matches[i].queryIdx].pt[1]
            x2 = keypoints_2[matches[i].trainIdx].pt[0]
            y2 = keypoints_2[matches[i].trainIdx].pt[1]

            # good matches have to satisfy the criteria
            if x1 + tolerancy_lr >= x2 and x1 - tolerancy_lr <= x2 and y1 + tolerancy_tb >= y2 and y1 - tolerancy_tb <= y2:
                good_matches.append(matches[i])

        if len(good_matches):
            if self.show_results:
                img_matches_good = cv2.drawMatches(img_1, keypoints_1, img_2, keypoints_2, good_matches, None, matchColor=(0, 255, 0), singlePointColor=(0, 0, 255), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
                cv2.imshow('Good matches', img_matches_good)

            if self.save_results:
                img_matches_good = cv2.drawMatches(img_1, keypoints_1, img_2, keypoints_2, good_matches, None, matchColor=(0, 255, 0), singlePointColor=(0, 0, 255), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
                cv2.imwrite(path + '/03_god_matches.png', img_matches_good)

        return good_matches










