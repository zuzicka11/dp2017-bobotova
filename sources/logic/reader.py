import os
import SimpleITK as sitk
from objects.volume import Volume
from objects.examination import Examination


# class for reading all patient examinations from path
class Reader:

    def read(self, patient_examination_path):

        examination = Examination()

        examination.path = patient_examination_path
        examination.name = patient_examination_path.split('/')[-1]

        files = self.__find_files(patient_examination_path) # find all mha and nii files in all patient examinations
        examination.mri_t1 = self.__load_data(self.__find_path_to('.MR_T1.', files))    # find path to T1 modality and create Volume
        examination.mri_t1c = self.__load_data(self.__find_path_to('.MR_T1c.', files))  # find path to T1c modality and create Volume
        examination.mri_t2 = self.__load_data(self.__find_path_to('.MR_T2.', files))    # find path to T2 modality and create Volume
        examination.mri_flair = self.__load_data(self.__find_path_to('Flair', files))   # find path to flair modality and create Volume
        examination.segmentation = self.__load_data(self.__find_path_to('3more', files))# find path to segmentation and create Volume

        return examination

    # find all files in subfolders of patient examination main folder which end with mha or nii
    def __find_files(self, path):
        resulting_files = []
        for (path, dirs, files) in os.walk(path):
            for file in files:
                if file.endswith(".mha") or file.endswith(".nii"):
                    resulting_files.append(os.path.join(path, file))

        return resulting_files

    # find path to concrete file - depending on modality
    def __find_path_to(self, modality, files):
        for file in files:
            if file.find(modality) is not -1:
                return file
        return -1

    # load data from file and save them to Volume object
    def __load_data(self, path):
        if path is not -1:
            print("Nacitavam:", path)
            sitk_object = sitk.ReadImage(path)
            volume = Volume(sitk_object)

            return volume
        else:
            print("Nespravna cesta k suboru v logic.data_handler.load_data")
