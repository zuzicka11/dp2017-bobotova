import numpy as np
import SimpleITK as sitk
import matplotlib.pyplot as plt
import os
import math

class Preprocessing:

    def __init__(self, template=None, remove_percentile=False, bottom_percentile=5, top_percentile=99, ignore_zero_values=False, help_print=False, save_results=False, save_vol=False, path=""):

        if save_results and path != "":
            if not os.path.exists(path):
                os.makedirs(path)

        self.ignore_zero_values = ignore_zero_values
        self.help_print = help_print
        self.save_results = save_results
        self.want_save_volume = save_vol
        self.path = path
        self.bottom_percentile = bottom_percentile
        self.top_percentile = top_percentile
        self.rem_percentile = remove_percentile

        if np.amin(template) < 0:
            print("Preprocessing: pocet odstranenych zapornych hodnot: ", len(template[np.where(template < 0)]))
            template[np.where(template < 0)] = 0

        if self.help_print:
            # basic values template volume
            print("Template: ")
            print("- minimum:", np.amin(template))
            print("- maximum:", np.amax(template))
            print("- percentil " + str(bottom_percentile), np.percentile(template, bottom_percentile))
            print("- percentil " + str(top_percentile), np.percentile(template, top_percentile))
            print("- median", np.median(template))
            print("--------------")

        if self.ignore_zero_values:
            tmp = np.ma.masked_where(template == 0, template)
            tmp = tmp.compressed()

            if self.help_print:
                # basic values after ignoring background
                print("Ignore zero values in template:")
                print("- minimum:", np.amin(tmp))
                print("- maximum:", np.amax(tmp))
                print("- percentil " + str(bottom_percentile), np.percentile(tmp, bottom_percentile))
                print("- percentil " + str(top_percentile), np.percentile(tmp, top_percentile))
                print("- median", np.median(template))
                print("--------------")
        else:
            tmp = None

        if template is not None:

            template_original = template.copy()
            if self.rem_percentile:
                template = self.remove_percentile(data=template, data_info=tmp, bottom_percentile=self.bottom_percentile, top_percentile=self.top_percentile)
            template = self.normalize(data=template)

            if self.help_print or self.save_results:
                if self.rem_percentile:
                    self.paint_hist_after_scale_and_norm(original=template_original, scaled=template, path=path)
                else:
                    self.paint_hist_after_norm(original=template, path=path)

            self.template = template

            if self.want_save_volume:
                if not self.save_volume(data=template, path=path, name="_normalized"):
                    print("Can not save the volume.")

    def get_processed_teplate(self):
        return self.template.astype(np.uint8)

    def process_data(self, data, path=None):

        if self.save_results and path is not None:
            if not os.path.exists(path):
                os.makedirs(path)

        if np.amin(data) < 0:
            print("Preprocessing: pocet odstranenych zapornych hodnot: ", len(data[np.where(data < 0)]))
            data[np.where(data < 0)] = 0

        if self.help_print:
            # basic values of volume
            print("Volume:")
            print("- minimum:", np.amin(data))
            print("- maximum:", np.amax(data))
            print("- percentil " + str(self.bottom_percentile), np.percentile(data, self. bottom_percentile))
            print("- percentil " + str(self.top_percentile), np.percentile(data, self.top_percentile))
            print("- median", np.median(data))

        if self.ignore_zero_values:
            # ignore black backround - zero values
            tmp = np.ma.masked_where(data == 0, data)
            tmp = tmp.compressed()

            if self.help_print:
                # basic values after ignoring background
                print("Ignore zero values in template:")
                print("Volume minimum:", np.amin(data))
                print("Volume maximum:", np.amax(data))
                print("Volume percentil " + str(self.bottom_percentile), np.percentile(data, self.bottom_percentile))
                print("Volume percentil " + str(self.top_percentile), np.percentile(data, self.top_percentile))
                print("Volume median", np.median(data))
        else:
            tmp = None

        data_original = data.copy()
        if self.rem_percentile:
            data = self.remove_percentile(data=data, data_info=tmp, bottom_percentile=self.bottom_percentile, top_percentile=self.top_percentile)
        data = self.normalize(data=data)

        if self.help_print or self.save_results:
            if self.rem_percentile:
                self.paint_hist_after_scale_and_norm(original=data_original, scaled=data, path=path)
            else:
                self.paint_hist_after_norm(original=data, path=path)

        data_original = data.copy()
        data = self.hist_match(template=self.template, data=data)

        if self.help_print or self.save_results:
            self.paint_hist_after_hist_match(scaled_template=self.template, scaled_data=data_original, matched_data=data, path=path)

        if self.want_save_volume:
            if path is None:
                path = self.path
            if not self.save_volume(data=data, path=path, name="_matched"):
                print("Can not save the volume.")

        return self.normalize(data)

    # scale volume / image
    def remove_percentile(self, data, data_info=None, bottom_percentile=10, top_percentile=99):

        if data_info is not None:
            new_min = np.percentile(data_info, bottom_percentile)
            new_max = np.percentile(data_info, top_percentile)
        else:
            new_min = np.percentile(data, bottom_percentile)
            new_max = np.percentile(data, top_percentile)

        data[np.where(data < new_min)] = new_min
        data[np.where(data > new_max)] = new_max

        if self.help_print:
            print("- scaled: new_min", new_min, "new_max", new_max)
            print("- scaled data minimum:", np.amin(data))
            print("- scaled data maximum:", np.amax(data))
            print("--------------")

        return data

    # normalize volume / image
    def normalize(self, data):

        new_min = np.amin(data)
        new_max = np.amax(data)

        print(new_min, new_max)

        normalized = ((data - new_min) / (new_max - new_min)) * 255 #65535

        print(np.amin(normalized), np.amax(data))

        if self.help_print:
            print("- normalized: min:", new_min, "max", new_max)
            print("- normalized data minimum:", np.amin(normalized))
            print("- normalized data maximum:", np.amax(normalized))
            print("--------------")

        return np.uint8(normalized)

    def hist_match(self, template, data):
        """
        Adjust the pixel values of a grayscale image such that its histogram
        matches that of a target image

        Arguments:
        -----------
            data: np.ndarray
                Image to transform; the histogram is computed over the flattened
                array
            template: np.ndarray
                Template image; can have different dimensions to data
        Returns:
        -----------
            matched: np.ndarray
                The transformed output image
        """

        oldshape = data.shape
        data = data.ravel()
        template = template.ravel()

        # get the set of unique pixel values and their corresponding indices and
        # counts
        s_values, bin_idx, s_counts = np.unique(data, return_inverse=True,
                                                return_counts=True)
        t_values, t_counts = np.unique(template, return_counts=True)

        # take the cumsum of the counts and normalize by the number of pixels to
        # get the empirical cumulative distribution functions for the data and
        # template images (maps pixel value --> quantile)
        s_quantiles = np.cumsum(s_counts).astype(np.float64)
        s_quantiles /= s_quantiles[-1]
        t_quantiles = np.cumsum(t_counts).astype(np.float64)
        t_quantiles /= t_quantiles[-1]

        # interpolate linearly to find the pixel values in the template image
        # that correspond most closely to the quantiles in the data image
        interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

        return interp_t_values[bin_idx].reshape(oldshape)


    def paint_hist_after_norm(self, original, path=None):

        maximum = np.amax(original)
        minimum = np.amin(original)

        bins = np.arange(minimum, maximum)
        frq, edges = np.histogram(original, bins)

        plt.rcParams['axes.grid'] = True
        fig, ax1 = plt.subplots(1, sharex=True, sharey=True)
        ax1.bar(edges[:-1], frq, width=np.diff(edges), ec="k", align="edge")
        ax1.set_title('Normalized data histogram')

        frq.sort()
        plt.axis([minimum, maximum, 0, frq[-2]])
        plt.grid(True)

        if self.help_print:
            plt.show()
        if self.save_results:
            if path is None:
                path = self.path
            plt.savefig(path+'/normalized.png')


    def paint_hist_after_scale_and_norm(self, original, scaled, path=None):

        if np.amax(original) > np.amax(scaled):
            maximum = np.amax(original)
        else:
            maximum = np.amax(scaled)

        if np.amin(original) < np.amin(scaled):
            minimum = np.amin(original)
        else:
            minimum = np.amin(scaled)

        bins = np.arange(minimum, maximum)
        frq, edges = np.histogram(original, bins)
        frq1, edges1 = np.histogram(scaled, bins)

        plt.rcParams['axes.grid'] = True
        fig, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True)
        ax1.bar(edges[:-1], frq, width=np.diff(edges), ec="k", align="edge")
        ax1.set_title('a) original data histogram')
        ax2.bar(edges1[:-1], frq1, width=np.diff(edges1), ec="k", align="edge")
        ax2.set_title('b) scaled and normalized data histogram')

        frq.sort()
        frq1.sort()

        if frq[-2] > frq1[-2]:
            y_max = frq[-2]
        else:
            y_max = frq1[-2]

        plt.axis([minimum, maximum, 0, y_max])
        plt.grid(True)

        if self.help_print:
            plt.show()
        if self.save_results:
            if path is None:
                path = self.path
            plt.savefig(path+'/original_vs_normalized.png')

    def paint_hist_after_hist_match(self, scaled_template, scaled_data, matched_data, path=None):
        scaled_template_tmp = scaled_template.copy()
        scaled_data_tmp = scaled_data.copy()
        matched_data_tmp = matched_data.copy()

        scaled_template = ((scaled_template-np.amin(scaled_template_tmp))/(np.amax(scaled_template_tmp)-np.amin(scaled_template_tmp)))*1024
        scaled_data = ((scaled_data-np.amin(scaled_data_tmp))/(np.amax(scaled_data_tmp)-np.amin(scaled_data_tmp)))*1024
        matched_data = ((matched_data-np.amin(matched_data_tmp))/(np.amax(matched_data_tmp)-np.amin(matched_data_tmp)))*1024

        if np.amax(scaled_template) > np.amax(scaled_data) and np.amax(scaled_template) > np.amax(matched_data):
            maximum = np.amax(scaled_template)
        elif np.amax(scaled_data) > np.amax(matched_data):
            maximum = np.amax(scaled_data)
        else:
            maximum = np.amax(matched_data)

        if np.amin(scaled_template) < np.amin(scaled_data) and np.amin(scaled_template) < np.amin(matched_data):
            minimum = np.amin(scaled_template)
        elif np.amax(scaled_data) < np.amax(matched_data):
            minimum = np.amin(scaled_data)
        else:
            minimum = np.amin(matched_data)

        bins = np.arange(minimum, maximum)

        frq, edges = np.histogram(scaled_data, bins)
        frq1, edges1 = np.histogram(scaled_template, bins)
        frq2, edges2 = np.histogram(matched_data, bins)

        plt.rcParams['axes.grid'] = True
        fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True, figsize=(8, 8))
        ax1.bar(edges[:-1], frq, width=np.diff(edges), ec="k", align="edge")
        ax1.set_title('a) original data histogram')
        ax2.bar(edges1[:-1], frq1, width=np.diff(edges1), ec="k", align="edge")
        ax2.set_title('b) template histogram')
        ax3.bar(edges2[:-1], frq2, width=np.diff(edges2), ec="k", align="edge")
        ax3.set_title('c) matched data histogram')

        frq.sort()
        frq1.sort()
        frq2.sort()
        if frq[-2] > frq1[-2] and frq[-2] > frq2[-2]:
            y_max = frq[-2]
        elif frq1[-2] > frq2[-2]:
            y_max = frq1[-2]
        else:
            y_max = frq2[-2]

        plt.axis([minimum, maximum, 0, y_max])

        if self.help_print:
            plt.show()
        if self.save_results:
            if path is None:
                path = self.path
            plt.savefig(path+'/original_vs_template_vs_matched.png')

    def save_volume(self, data, path=None, name="new"):
        sitk_result = sitk.GetImageFromArray(data)
        if path is None:
            path = self.path
        sitk.WriteImage(sitk_result, path+name+".mha")

        return 1
