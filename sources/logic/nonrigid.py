import numpy as np
import cv2
import os
import copy


# class to register volumes per frames non-rigidly - for tumor changes tracking
class NonRigidRegistration:

    def __init__(self, registration_type, show_results=False, save_results=False, save_all_results=False, farneback_params=dict(pyr_scale=0.5, levels=3, winsize=15, iterations=3, poly_n=5, poly_sigma=1.2, flags=0), feature_params=dict(maxCorners=100, qualityLevel=0.3, minDistance=7, blockSize=7), lk_params=dict(winSize=(15, 15), maxLevel=2, criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))):

        # default setting
        self.show_results = show_results    # if want save results
        self.save_results = save_results    # if want save important results
        self.save_all_results = save_all_results # if want save all results
        self.registration_type = registration_type  # choose registration type (Farneback or LK)

        if self.registration_type == 'LK': # set params for LK optical flow
            # params for ShiTomasi corner detection
            self.feature_params = feature_params

            # parameters for lucas kanade optical flow
            self.lk_params = lk_params

            if self.show_results or self.save_results or self.save_all_results:
                # Create some random colors
                self.color = np.random.randint(0, 255, (100, 3))

        elif self.registration_type == "Farneback": # set params for Farneback optical flow
            # parameters for farneback optical flow
            self.farneback_params = farneback_params

        else:
            print("Nonrigid - neznamy typ registracie.")

    # function returns points of old examination frame and actual examination frame computed from flow
    def __get_points_from_flow(self, img, flow, step=8):
        h, w = flow.shape[:2]
        y, x = np.mgrid[step / 2:h:step, step / 2:w:step].reshape(2, -1).astype(int)
        fx, fy = flow[y, x].T
        lines = np.vstack([x, y, x + fx, y + fy]).T.reshape(-1, 2, 2)
        lines = np.int32(lines + 0.5)

        old_points = []
        new_points = []

        for (x1, y1), (_x2, _y2) in lines:
            if img[y1][x1] > 0 and img[_y2][_x2] > 0: # only points inside the brain are accepted
                old_points.append((x1, y1))
                new_points.append((_x2, _y2))

        return old_points, new_points

    # function draw flow - motions between examination frames after farneback
    def __draw_flow_after_farneback(self, img, old_points, new_points):

        vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

        for (x1, y1), (_x2, _y2) in zip(old_points, new_points):
            cv2.arrowedLine(vis, (x1, y1), (_x2, _y2), (0, 255, 0), 1)
            cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)

        return vis

    # function draw HSV after farneback - it shows motion between two examinations
    def __draw_hsv_after_farneback(self, flow):

        h, w = flow.shape[:2]
        fx, fy = flow[:, :, 0], flow[:, :, 1]
        ang = np.arctan2(fy, fx) + np.pi
        v = np.sqrt(fx*fx+fy*fy)
        hsv = np.zeros((h, w, 3), np.uint8)
        hsv[..., 0] = ang*(180/np.pi/2)
        hsv[..., 1] = 255
        hsv[..., 2] = np.minimum(v*4, 255)
        bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

        return bgr

    # function draw motions between examination frames after lucas-canade optical flow
    def __draw_tracks_after_lk(self, actual_img, good_old, good_new, mask):
        # draw the tracks
        for i, (new, old) in enumerate(zip(good_new, good_old)):
            a, b = new.ravel()
            c, d = old.ravel()
            mask = cv2.line(mask, (a, b), (c, d), self.color[i].tolist(), 2)

        img = cv2.add(actual_img, mask)

        return mask, img

    # function process all corresponding frames of examination volumes using lucas-canade optical flow
    def process_frames_lk(self, frames, paths=None, path_extension=None, modality='flair'):

        old_gray = None
        p0 = None
        mask = None
        warped_frames = []
        old_points_list = []
        new_points_list = []

        for index, frame in enumerate(frames):

            result = None
            warped_result = []
            old_points = []
            new_points = []
            save_path = ""

            if old_gray is None and p0 is None:
                # take first frame
                old_gray = frame.copy()

                # find corners on first frame
                p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **self.feature_params)

                if self.show_results or self.save_results or self.save_all_results:
                    # create a mask image for drawing purposes
                    mask = np.zeros_like(frame)

            else:
                # create folders for saving results
                if self.save_results or self.save_all_results:
                    save_path = paths[index-1]
                    save_path += '/lk_'+modality
                    if not os.path.exists(save_path):
                        os.makedirs(save_path)
                    save_path += '/'+path_extension
                    if not os.path.exists(save_path):
                        os.makedirs(save_path)

                # actual image
                actual_gray = frame.copy()

                if self.show_results:
                    cv2.imshow('frame_old', old_gray)
                    cv2.imshow('frame_actual', actual_gray)
                if self.save_results or self.save_all_results:
                    cv2.imwrite(save_path+'/01a_frame_old.png', old_gray)
                    cv2.imwrite(save_path+'/01b_frame_actual.png', actual_gray)

                # calculate optical flow - Lucas Kanade
                p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, actual_gray, p0, None, **self.lk_params)

                # select good points
                good_old_points = p0[st == 1]
                good_actual_points = p1[st == 1]

                old_points = list(tuple(map(tuple, good_old_points)))
                new_points = list(tuple(map(tuple, good_actual_points)))

                if self.show_results or self.save_all_results:
                    vis_points_img = np.zeros(old_gray.shape, dtype=old_gray.dtype)
                    vis_points_img = cv2.cvtColor(vis_points_img, cv2.COLOR_GRAY2BGR)
                    for p1, p2 in zip(old_points, new_points):
                        cv2.circle(vis_points_img, p1, 1, (255, 0, 0), -1)
                        cv2.circle(vis_points_img, p2, 1, (0, 255, 0), -1)

                    if self.show_results:
                        cv2.imshow('old and new points', vis_points_img)
                    if self.save_all_results:
                        cv2.imwrite(save_path+'/02_blue_old_and_green_new_points.png', vis_points_img)

                if self.show_results or self.save_results or self.save_all_results:
                    mask, result = self.__draw_tracks_after_lk(actual_img=frame, good_old=good_old_points, good_new=good_actual_points, mask=mask)

                    if self.show_results:
                        cv2.imshow('PyrLK', result)
                    if self.save_results or self.save_all_results:
                        cv2.imwrite(save_path+'/03_optical_flow_LK.png', result)

                # warp old image to actual image for evaluation using Delaunay
                warped_result = self.warp_image(img_old=old_gray.copy(), img_new=actual_gray.copy(), old_points=old_points, new_points=new_points, path=save_path)
                # morph images for visualization
                self.morph_images(img_old=old_gray.copy(), img_new=actual_gray.copy(), old_points=old_points, new_points=new_points, path=save_path)

                # update the previous frame and previous points
                old_gray = actual_gray.copy()
                p0 = good_actual_points.reshape(-1, 1, 2)

            if result is not None:
                warped_frames.append(warped_result)
                old_points_list.append(old_points)
                new_points_list.append(new_points)

        return warped_frames, old_points_list, new_points_list

    # function process all corresponding frames of examination volumes using farneback optical flow
    def process_frames_farneback(self, frames, paths=None, path_extension=None, modality='flair'):

        old_gray = None
        warped_frames = []
        old_points_list = []
        new_points_list = []

        for index, frame in enumerate(frames):

            result = None
            old_points = None
            new_points = None
            save_path = ""

            if old_gray is None:
                # take first frame
                old_gray = frame.copy()

            else:
                # create folders for saving results
                if self.save_results or self.save_all_results:
                    save_path = paths[index-1]
                    save_path += '/farneback_'+modality
                    if not os.path.exists(save_path):
                        os.makedirs(save_path)
                    save_path += '/'+path_extension
                    if not os.path.exists(save_path):
                        os.makedirs(save_path)

                # actual image
                actual_gray = frame.copy()

                if self.show_results:
                    cv2.imshow('frame_old', old_gray)
                    cv2.imshow('frame_actual', actual_gray)
                if self.save_results or self.save_all_results:
                    cv2.imwrite(save_path+'/01a_frame_old.png', old_gray)
                    cv2.imwrite(save_path+'/01b_frame_actual.png', actual_gray)

                # calc optical flow using Farneback
                flow = cv2.calcOpticalFlowFarneback(old_gray, actual_gray, None, **self.farneback_params)
                # get old (points of old_gray image) and new points (points of actual grey_image) from flow
                old_points, new_points = self.__get_points_from_flow(img=actual_gray, flow=flow, step=16)

                if self.show_results or self.save_all_results:
                    vis_points_img_old = cv2.cvtColor(old_gray.copy(), cv2.COLOR_GRAY2BGR)
                    vis_points_img_new = cv2.cvtColor(actual_gray.copy(), cv2.COLOR_GRAY2BGR)
                    for p1, p2 in zip(old_points, new_points):
                        cv2.circle(vis_points_img_old, p1, 1, (255, 0, 0), -1)
                        cv2.circle(vis_points_img_new, p2, 1, (0, 255, 0), -1)

                    if self.show_results:
                        cv2.imshow('old points', vis_points_img_old)
                        cv2.imshow('new points', vis_points_img_new)
                    if self.save_all_results:
                        cv2.imwrite(save_path+'/02a_blue_old_points.png', vis_points_img_old)
                        cv2.imwrite(save_path+'/02b_green_new_points.png', vis_points_img_new)

                if self.show_results or self.save_results or self.save_all_results:
                    # draw flow
                    flow = self.__draw_flow_after_farneback(img=old_gray, old_points=old_points, new_points=new_points)
                    # draw hsv
                    hsv = self.__draw_hsv_after_farneback(flow=flow)

                    if self.show_results:
                        cv2.imshow('Farneback flow', flow)
                        cv2.imshow('Farneback hsv', hsv)
                    if self.save_results or self.save_all_results:
                        cv2.imwrite(save_path+'/03a_optical_flow_FARNEBACK.png', flow)
                        cv2.imwrite(save_path+'/03b_optical_flow_FARNEBACK_HSV.png', hsv)

                # warp old image to actual image for evaluation using Delaunay
                result = self.warp_image(img_old=old_gray.copy(), img_new=actual_gray.copy(), old_points=old_points, new_points=new_points, animate=False, path=save_path)
                # morph images for visualization
                self.morph_images(img_old=old_gray.copy(), img_new=actual_gray.copy(),  old_points=old_points, new_points=new_points, animate=False, path=save_path)

                old_gray = actual_gray.copy()

            if result is not None:
                warped_frames.append(result)
                old_points_list.append(old_points)
                new_points_list.append(new_points)

        return warped_frames, old_points_list, new_points_list

    # process all examinations of one patient
    def process_volumes(self, examinations, modality='flair'):

        length = 0

        # get volume from examinations depending on selected modality
        for examination in examinations:
            if modality == 'flair':
                volume = examination.mri_flair.data
            if modality == 't1':
                volume = examination.mri_t1.data
            if modality == 't1c':
                volume = examination.mri_t1c.data
            if modality == 't2':
                volume = examination.mri_t2.data

            # find max length of volumes
            length = len(volume)

            if len(volume) > length:
                print("nonrigid volume length problem")
                length = len(volume)

        # for all slices in examination volumes calculate optical flow
        for index in range(0, length):
            processed_frames = []
            old_points_list = []
            new_points_list = []
            frames = []
            paths = []
            # get corresponding slice from all examinations
            for examination in examinations:
                # get volume from examinations depending on selected modality
                if modality == 'flair':
                    vol = examination.mri_flair
                    volume = examination.mri_flair.data
                if modality == 't1':
                    vol = examination.mri_t1
                    volume = examination.mri_t1.data
                if modality == 't1c':
                    vol = examination.mri_t1c
                    volume = examination.mri_t1c.data
                if modality == 't2':
                    vol = examination.mri_t2
                    volume = examination.mri_t2.data

                tmp_frame = volume[index]

                # compute only bigger brain parts
                if len(tmp_frame[np.where(tmp_frame > 0)]) > 5000:
                    frames.append(tmp_frame)
                    paths.append(examination.path)

            if len(examinations) == len(frames):
                if self.registration_type == 'LK':
                    # run Lucas-Kanada optical flow
                    processed_frames, old_points_list, new_points_list = self.process_frames_lk(frames=frames, paths=paths, path_extension=str(index), modality=modality)
                elif self.registration_type == "Farneback":
                    # run Farneback optical flow
                    processed_frames, old_points_list, new_points_list = self.process_frames_farneback(frames=frames, paths=paths, path_extension=str(index), modality=modality)

            # create resulting examination with warped frames
            if len(processed_frames):
                for i, examination in enumerate(examinations[:-1]):

                    if examination.warped_data is None:
                        examination.warped_data = copy.deepcopy(vol)
                        examination.warped_data.data = np.uint8(examination.warped_data.data)

                    examination.warped_data.data[index] = processed_frames[i]
                    examination.own_points = old_points_list[i]
                    examination.next_points = new_points_list[i]

        return examinations

    # function detects if point is inside the rectangle
    def __rect_contains__(self, rect, point):
        if point[0] < rect[0]:
            return False
        elif point[1] < rect[1]:
            return False
        elif point[0] > rect[2]:
            return False
        elif point[1] > rect[3]:
            return False
        return True

    # paint triangles reached from Delaunay triangulation
    def paint_triangles(self, img, triangles, color):
        for t in triangles:
            cv2.line(img, t[0], t[1], color, 1, cv2.LINE_AA, 0)
            cv2.line(img, t[1], t[2], color, 1, cv2.LINE_AA, 0)
            cv2.line(img, t[2], t[0], color, 1, cv2.LINE_AA, 0)

        return img

    # paint step of Delaunay triangulation
    def __draw_delaunay_in_animation__(self, img, subdiv, delaunay_color):

        triangleList = subdiv.getTriangleList()
        size = img.shape
        r = (0, 0, size[1], size[0])

        for t in triangleList:

            pt1 = (t[0], t[1])
            pt2 = (t[2], t[3])
            pt3 = (t[4], t[5])

            if self.__rect_contains__(r, pt1) and self.__rect_contains__(r, pt2) and self.__rect_contains__(r, pt3):
                cv2.line(img, pt1, pt2, delaunay_color, 1, cv2.LINE_AA, 0)
                cv2.line(img, pt2, pt3, delaunay_color, 1, cv2.LINE_AA, 0)
                cv2.line(img, pt3, pt1, delaunay_color, 1, cv2.LINE_AA, 0)

    # compute delaunay triangulation
    def delaunay_triangulation(self, points, img, animate=True, path=None):

        # keep a copy of input image
        img_orig = img.copy()

        # create rectangle for Subdiv2D
        size = img.shape
        rect = (0, 0, size[1], size[0])

        # create an instance of Subdiv2D
        subdiv = cv2.Subdiv2D(rect)

        for idx, p in enumerate(points):
            # insert point to the SubDiv2D
            subdiv.insert(p)

            if animate:
                img_copy = img_orig.copy()
                # draw delaunay triangles
                self.__draw_delaunay_in_animation__(img_copy, subdiv, (255, 255, 255))

                if self.show_results:
                    cv2.imshow("Delaunay", img_copy)
                    cv2.waitKey(100)

                if self.save_all_results:
                    cv2.imwrite(path+"/delaunay_animate_"+str(idx)+".png", img_copy)

        # get triangles from subdiv
        triangleList = subdiv.getTriangleList()
        size = img.shape
        r = (0, 0, size[1], size[0])

        # select only good triangles and save as lst of points
        triangles = []
        for t in triangleList:

            pt1 = (int(t[0]), int(t[1]))
            pt2 = (int(t[2]), int(t[3]))
            pt3 = (int(t[4]), int(t[5]))

            if self.__rect_contains__(r, pt1) and self.__rect_contains__(r, pt2) and self.__rect_contains__(r, pt3):
                triangles.append([pt1, pt2, pt3])

        return triangles

    # transform one triangle to another one
    def apply_transform(self, src, srcTri, dstTri, size):

        # pair of triangles is used to find the affine transform
        warpMat = cv2.getAffineTransform(np.float32(srcTri), np.float32(dstTri))

        # apply affine transform
        dst = cv2.warpAffine(src, warpMat, (size[0], size[1]), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101)

        return dst

    # warp old image to the actual image depending on the Delaunay triangulation
    def warp_image(self, img_old, img_new, old_points, new_points, animate=False, path=None):

        # get corners of brain to add them to the point sets
        r = cv2.boundingRect(img_old.copy())
        b1 = (r[0], r[1])
        b2 = (r[0]+r[2], r[1])
        b3 = (r[0]+r[2], r[1]+r[3])
        b4 = (r[0], r[1]+r[3])

        if self.show_results or self.save_results or self.save_all_results:
            img_to_paint_1 = np.zeros(img_old.shape, dtype=img_old.dtype)
            img_to_paint = img_old.copy()

            cv2.line(img_to_paint, b1, b2, (255, 255, 255), 1, cv2.LINE_AA, 0)
            cv2.line(img_to_paint, b2, b3, (255, 255, 255), 1, cv2.LINE_AA, 0)
            cv2.line(img_to_paint, b3, b4, (255, 255, 255), 1, cv2.LINE_AA, 0)
            cv2.line(img_to_paint, b4, b1, (255, 255, 255), 1, cv2.LINE_AA, 0)

            if self.show_results:
                cv2.imshow("bounding rect", img_to_paint)
            if self.save_all_results:
                cv2.imwrite(path+'/04a_warp_corners.png', img_to_paint)

            cv2.circle(img_to_paint, b1, 1, (255, 255, 255), -1)
            cv2.circle(img_to_paint, b2, 1, (255, 255, 255), -1)
            cv2.circle(img_to_paint, b3, 1, (255, 255, 255), -1)
            cv2.circle(img_to_paint, b4, 1, (255, 255, 255), -1)

            cv2.circle(img_to_paint_1, b1, 1, (255, 255, 255), -1)
            cv2.circle(img_to_paint_1, b2, 1, (255, 255, 255), -1)
            cv2.circle(img_to_paint_1, b3, 1, (255, 255, 255), -1)
            cv2.circle(img_to_paint_1, b4, 1, (255, 255, 255), -1)

            if self.show_results:
                cv2.imshow("bounding rect + points", img_to_paint)
                cv2.imshow("bounding points", img_to_paint_1)
            if self.save_all_results:
                cv2.imwrite(path+'/04b_warp_rect_corner_points.png', img_to_paint)
                cv2.imwrite(path+'/04c_warp_corner_points.png', img_to_paint_1)

        # add corners to the point sets
        old_points.extend((b1, b2, b3, b4))
        new_points.extend((b1, b2, b3, b4))

        # compute weighted average point coordinates
        points = []
        alpha = 0.5

        for p1, p2 in zip(old_points, new_points):
            x = (1 - alpha) * p1[0] + alpha * p2[0]
            y = (1 - alpha) * p1[1] + alpha * p2[1]
            points.append((int(x), int(y)))

        if self.show_results or self.save_all_results or self.save_results:
            vis_points_img = np.zeros(img_old.shape, dtype=img_old.dtype)
            vis_points_img = cv2.cvtColor(vis_points_img, cv2.COLOR_GRAY2BGR)

            for p1 in old_points:
                cv2.circle(vis_points_img, p1, 1, (255, 0, 0), -1)

            if self.show_results:
                cv2.imshow('old points', vis_points_img)
            if self.save_all_results:
                cv2.imwrite(path+'/05a_warp_blue_old_points.png', vis_points_img)

            vis_points_img_help = np.zeros(img_old.shape, dtype=img_old.dtype)
            vis_points_img_help = cv2.cvtColor(vis_points_img_help, cv2.COLOR_GRAY2BGR)

            for p2 in new_points:
                cv2.circle(vis_points_img, p2, 1, (0, 255, 0), -1)
                cv2.circle(vis_points_img_help, p2, 1, (0, 255, 0), -1)

            if self.show_results:
                cv2.imshow('old and new points', vis_points_img)
                cv2.imshow('new points', vis_points_img_help)
            if self.save_all_results:
                cv2.imwrite(path+'/05c_warp_blue_old_and_green_new_points.png', vis_points_img)
                cv2.imwrite(path+'/05b_warp_green_new_points.png', vis_points_img_help)

            for p in points:
                cv2.circle(vis_points_img, p, 1, (255, 255, 255), -1)

            if self.show_results:
                cv2.imshow('old, new adn weighted points', vis_points_img)
            if self.save_results or self.save_all_results:
                cv2.imwrite(path+'/06_warp_blue_old_green_new_and_white_weighted.png', vis_points_img)

        img_tmp = np.zeros(img_old.shape, dtype=img_old.dtype)

        if animate:
            for p in points:
                cv2.circle(img_tmp, p, 1, (255, 255, 255), -1)

        # compute delaunay trinagulation on weighted points
        triangles = self.delaunay_triangulation(points=points, img=img_tmp, animate=animate, path=path)

        # get triangles of old point set and actual points set from triangles created using weighted points
        triangles_old = []
        triangles_new = []
        for t in triangles:
            p1_i = points.index(t[0])
            p2_i = points.index(t[1])
            p3_i = points.index(t[2])
            triangles_old.append([old_points[p1_i], old_points[p2_i], old_points[p3_i]])
            triangles_new.append([new_points[p1_i], new_points[p2_i], new_points[p3_i]])

        if self.show_results or self.save_results or self.save_all_results:
            triangles_vis = np.zeros(img_old.shape, dtype=img_old.dtype)
            triangles_vis = cv2.cvtColor(triangles_vis, cv2.COLOR_GRAY2BGR)
            triangles_vis = self.paint_triangles(img=triangles_vis, triangles=triangles, color=(255, 255, 255))

            if self.show_results:
                cv2.imshow("Delaunay final", triangles_vis)

            if self.save_results or self.save_all_results:
                cv2.imwrite(path+'/07a_delaunay_final.png', triangles_vis)

            triangles_vis1 = self.paint_triangles(img=triangles_vis.copy(), triangles=triangles_old, color=(255, 0, 0))
            img_old_bgr = cv2.cvtColor(img_old, cv2.COLOR_GRAY2BGR)
            triangles_vis_old = self.paint_triangles(img=img_old_bgr, triangles=triangles_old, color=(255, 0, 0))

            triangles_vis2 = self.paint_triangles(img=triangles_vis.copy(), triangles=triangles_new, color=(0, 255, 0))
            img_new_bgr = cv2.cvtColor(img_new, cv2.COLOR_GRAY2BGR)
            triangles_vis_new = self.paint_triangles(img=img_new_bgr, triangles=triangles_new, color=(0, 255, 0))

            if self.show_results:
                cv2.imshow("Delaunay final a", triangles_vis1)
                cv2.imshow("Delaunay final b", triangles_vis_old)
                cv2.imshow("Delaunay final c", triangles_vis2)
                cv2.imshow("Delaunay final d", triangles_vis_new)

            if self.save_all_results:
                cv2.imwrite(path+"/07b_delaunay_final_old.png", triangles_vis1)
                cv2.imwrite(path+"/07c_delaunay_final_old.png", triangles_vis_old)
                cv2.imwrite(path+"/07d_delaunay_final_new.png", triangles_vis2)
                cv2.imwrite(path+"/07e_delaunay_final_new.png", triangles_vis_new)

        # create warped image
        imgMorph = img_old.copy()

        idx = 0
        for t1, t2 in zip(triangles_old, triangles_new):

            # find bounding rectangle for triangles
            r1 = cv2.boundingRect(np.float32([t1]))
            r2 = cv2.boundingRect(np.float32([t2]))

            t1Rect = []
            t2Rect = []

            for i in range(0, 3):
                t1Rect.append(((t1[i][0] - r1[0]), (t1[i][1] - r1[1])))
                t2Rect.append(((t2[i][0] - r2[0]), (t2[i][1] - r2[1])))

            # get mask of processed triangle by filling triangle
            mask = np.zeros((r2[3], r2[2]), dtype=np.float32)
            cv2.fillConvexPoly(mask, np.int32(t2Rect), (1.0, 1.0, 1.0), 16, 0)

            img1Rect = img_old[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
            size = (r2[2], r2[3])
            warpImage1 = self.apply_transform(img1Rect, t1Rect, t2Rect, size)

            # copy triangular region of the rectangular patch to the output image
            if imgMorph[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]].shape[0] > 0 and imgMorph[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]].shape[1] > 0:
                imgMorph[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]] = imgMorph[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]] * (1 - mask) + warpImage1 * mask

            if animate:
                if self.show_results:
                    cv2.imshow("Warped image", imgMorph)
                    cv2.waitKey(100)

                if self.save_all_results:
                    cv2.imwrite(path+"/08_warping_image_"+str(idx)+".png", imgMorph)
                    idx += 1

        if self.show_results:
            cv2.imshow("Warped image", imgMorph)

        if self.save_results or self.save_all_results:
            cv2.imwrite(path+"/09_warped_image.png", imgMorph)

        return np.uint8(imgMorph)

    # morph old and actual image for visualization
    def morph_images(self, img_old, img_new, old_points, new_points, animate=False, path=None):

        alpha = 0.5

        # morph images without warping
        img_morphed = np.uint8((1.0 - alpha) * img_old + alpha * img_new)

        if self.show_results:
            cv2.imshow("Morphed image", img_morphed)

        if self.save_results or self.save_all_results:
            cv2.imwrite(path+"/10_morphed_only_image.png", img_morphed)

        r = cv2.boundingRect(img_old)
        b1 = (r[0], r[1])
        b2 = (r[0] + r[2], r[1])
        b3 = (r[0] + r[2], r[1] + r[3])
        b4 = (r[0], r[1] + r[3])

        if self.show_results or self.save_results or self.save_all_results:
            img_to_paint = img_old.copy()

            cv2.line(img_to_paint, b1, b2, (255, 255, 255), 1, cv2.LINE_AA, 0)
            cv2.line(img_to_paint, b2, b3, (255, 255, 255), 1, cv2.LINE_AA, 0)
            cv2.line(img_to_paint, b3, b4, (255, 255, 255), 1, cv2.LINE_AA, 0)
            cv2.line(img_to_paint, b4, b1, (255, 255, 255), 1, cv2.LINE_AA, 0)

        # compute weighted average point coordinates
        points = []

        old_points.extend((b1, b2, b3, b4))
        new_points.extend((b1, b2, b3, b4))

        for p1, p2 in zip(old_points, new_points):
            x = (1 - alpha) * p1[0] + alpha * p2[0]
            y = (1 - alpha) * p1[1] + alpha * p2[1]
            points.append((int(x), int(y)))

        # compute delaunay trinagulation on weighted points
        triangles = self.delaunay_triangulation(points=points, img=img_old, animate=animate, path=path)

        # get triangles of old point set and actual points set from triangles created using weighted points
        triangles_old = []
        triangles_new = []
        for t in triangles:
            p1_i = points.index(t[0])
            p2_i = points.index(t[1])
            p3_i = points.index(t[2])
            triangles_old.append([old_points[p1_i], old_points[p2_i], old_points[p3_i]])
            triangles_new.append([new_points[p1_i], new_points[p2_i], new_points[p3_i]])

        # morph images
        img_morph = img_old.copy()
        img_paint = np.zeros(img_old.shape, dtype=img_old.dtype)

        idx = 0
        for t, t1, t2 in zip(triangles, triangles_old, triangles_new):
            # find bounding rectangle for triangles
            r = cv2.boundingRect(np.float32([t]))
            r1 = cv2.boundingRect(np.float32([t1]))
            r2 = cv2.boundingRect(np.float32([t2]))

            tRect = []
            t1Rect = []
            t2Rect = []

            for i in range(0, 3):
                tRect.append(((t[i][0] - r[0]), (t[i][1] - r[1])))
                t1Rect.append(((t1[i][0] - r1[0]), (t1[i][1] - r1[1])))
                t2Rect.append(((t2[i][0] - r2[0]), (t2[i][1] - r2[1])))

            # get mask of processed triangle by filling triangle
            mask = np.zeros((r[3], r[2]), dtype=np.float32)
            cv2.fillConvexPoly(mask, np.int32(t2Rect), (1.0, 1.0, 1.0), 16, 0)

            img1Rect = img_old[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
            img2Rect = img_new[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]]

            size = (r[2], r[3])
            warpImage1 = self.apply_transform(img1Rect, t1Rect, tRect, size)
            warpImage2 = self.apply_transform(img2Rect, t2Rect, tRect, size)

            # alpha blending for rectangular patches
            imgRect = (1.0 - alpha) * warpImage1 + alpha * warpImage2

            # copy triangular region of the rectangular patch to the output image
            img_morph[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] = img_morph[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] * (1 - mask) + imgRect * mask
            img_paint[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] = img_paint[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] * (1 - mask) + imgRect * mask

            if animate:
                if self.show_results:
                    cv2.imshow("Transformed morphed image", img_paint)
                    cv2.waitKey(100)

                if self.save_all_results:
                    cv2.imwrite(path+"/11_morphing_image_"+str(idx)+".png", img_paint)
                    if idx == 10:
                        cv2.imwrite(path+"/11.5a_morphing_image_triangle_im1_" + str(idx) + ".png", img1Rect)
                        cv2.imwrite(path+"/11.5b_morphing_image_triangle_im2_" + str(idx) + ".png", img2Rect)
                        cv2.imwrite(path+"/11.5c_morphing_image_triangle_w1_" + str(idx) + ".png", warpImage1)
                        cv2.imwrite(path+"/11.5d_morphing_image_triangle_w2_" + str(idx) + ".png", warpImage2)
                        cv2.imwrite(path+"/11.5e_morphing_image_triangle_res_" + str(idx) + ".png", imgRect)
                    idx += 1

        if self.show_results or self.save_results or self.save_all_results:
            vis = self.paint_triangles(img=img_morph.copy(), triangles=triangles, color=(255, 255, 255))
            if self.show_results:
                cv2.imshow("Morphed image with triangles", vis)
            if self.save_results or self.save_all_results:
                cv2.imwrite(path+"/12_morphed_img_with_triangles.png", vis)

        if self.show_results:
            cv2.imshow("Transformed morphed image", img_morph)
            cv2.waitKey(100)
        if self.save_results or self.save_all_results:
            cv2.imwrite(path+"/13_morphed_transformed_image.png", img_morph)



