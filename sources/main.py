from PyQt4 import QtCore, QtGui
import SimpleITK as sitk
import vtk
import sys
import os
import numpy as np
import copy

from logic.reader import Reader
from logic.preprocessing import Preprocessing
from logic.nonrigid import NonRigidRegistration
from vizualization import gui
from vizualization import volume
from vizualization import histogram
from vizualization import axe
from vizualization import slices

# global variables
path_to_patient = "../data/pat_0"
patient_examinations = []  # object of patient examination contains t1, t2, flair and t1c volumes

class MyForm(QtGui.QMainWindow):

    def __init__(self, vis_volume, vis_segmentation, brain_path, segmentation_path, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.ui = gui.Ui_MainWindow()
        self.ui.setupUi(self)

        # model
        self.modelRenderer = vtk.vtkRenderer()
        self.ui.modelvtkWidget.GetRenderWindow().AddRenderer(self.modelRenderer)
        self.modelWindow = self.ui.modelvtkWidget.GetRenderWindow()
        self.modelInteractor = self.ui.modelvtkWidget.GetRenderWindow().GetInteractor()
        self.modelInteractor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

        self.tumorVolumeObject = volume.Volume(volume=vis_volume, segmentation=vis_segmentation)
        self.tumorModel = self.tumorVolumeObject.createTumorActor()
        self.modelRenderer.AddActor(self.tumorModel)

        self.brainVolumeObject = volume.Volume(volume=vis_volume, segmentation=vis_segmentation)
        self.healthBrainModel = self.brainVolumeObject.createBrainActor()
        self.modelRenderer.AddActor(self.healthBrainModel)

        # histogram health brain
        self.histogramHBRenderer = vtk.vtkRenderer()
        self.ui.histogramHBvtkWidget.GetRenderWindow().AddRenderer(self.histogramHBRenderer)
        self.histogramHBWindow = self.ui.histogramHBvtkWidget.GetRenderWindow()
        self.histogramHBInteractor = self.ui.histogramHBvtkWidget.GetRenderWindow().GetInteractor()
        self.histogramHBInteractor.SetInteractorStyle(vtk.vtkInteractorStyleImage())

        histogramHBObject = histogram.Histogram()
        self.histogramBrainActor = histogramHBObject.makeHistogram(self.brainVolumeObject.getBrainHist())
        for a in self.histogramBrainActor:
            self.histogramHBRenderer.AddActor(a)
        self.histogramHBInteractor.Initialize()

        # histogram tumor
        self.histogramTRenderer = vtk.vtkRenderer()
        self.ui.histogramTvtkWidget.GetRenderWindow().AddRenderer(self.histogramTRenderer)
        self.histogramTWindow = self.ui.histogramTvtkWidget.GetRenderWindow()
        self.histogramTInteractor = self.ui.histogramTvtkWidget.GetRenderWindow().GetInteractor()
        self.histogramTInteractor.SetInteractorStyle(vtk.vtkInteractorStyleImage())

        histogramTObject = histogram.Histogram()
        self.histogramTumorActor = histogramTObject.makeHistogram(self.tumorVolumeObject.getTumorHist())
        for a in self.histogramTumorActor:
            self.histogramTRenderer.AddActor(a)
        self.histogramTInteractor.Initialize()

        # axes object
        self.axeObject = axe.Axe()

        # axial
        self.axialRenderer = vtk.vtkRenderer()
        self.ui.axialvtkWidget.GetRenderWindow().AddRenderer(self.axialRenderer)
        self.axialWindow = self.ui.axialvtkWidget.GetRenderWindow()
        self.axialInteractor = self.ui.axialvtkWidget.GetRenderWindow().GetInteractor()
        self.axialInteractor.SetInteractorStyle(vtk.vtkInteractorStyleImage())

        self.axialSlicerObject = slices.Slicer(self.modelWindow, self.modelRenderer, self.axeObject)
        actorAxialBrain = self.axialSlicerObject.getBrainSlices("axial", brain_path=brain_path)
        self.axialRenderer.AddActor(actorAxialBrain)
        self.actorAxialTumor = self.axialSlicerObject.getTumorSlices("axial", segmentation_path=segmentation_path)
        #self.axialRenderer.AddActor(self.actorAxialTumor)

        self.axialInteractor.Initialize()
        self.axialSlicerObject.setInteraction(self.axialInteractor, self.axialWindow, self.ui.axialLabel)

        # sagittal
        self.sagittalRenderer = vtk.vtkRenderer()
        self.ui.sagittalvtkWidget.GetRenderWindow().AddRenderer(self.sagittalRenderer)
        self.sagittalWindow = self.ui.sagittalvtkWidget.GetRenderWindow()
        self.sagittalInteractor = self.ui.sagittalvtkWidget.GetRenderWindow().GetInteractor()
        self.sagittalInteractor.SetInteractorStyle(vtk.vtkInteractorStyleImage())

        self.sagittalSlicerObject = slices.Slicer(self.modelWindow, self.modelRenderer, self.axeObject)
        actorSagittalBrain = self.sagittalSlicerObject.getBrainSlices("sagittal", brain_path=brain_path)
        self.sagittalRenderer.AddActor(actorSagittalBrain)
        self.actorSagittalTumor = self.sagittalSlicerObject.getTumorSlices("sagittal", segmentation_path=segmentation_path)
        #self.sagittalRenderer.AddActor(self.actorSagittalTumor)

        self.sagittalInteractor.Initialize()
        self.sagittalSlicerObject.setInteraction(self.sagittalInteractor, self.sagittalWindow, self.ui.sagittalLabel)

        # oblique
        self.obliqueRenderer = vtk.vtkRenderer()
        self.ui.obliquevtkWidget.GetRenderWindow().AddRenderer(self.obliqueRenderer)
        self.obliqueWindow = self.ui.obliquevtkWidget.GetRenderWindow()
        self.obliqueInteractor = self.ui.obliquevtkWidget.GetRenderWindow().GetInteractor()
        self.obliqueInteractor.SetInteractorStyle(vtk.vtkInteractorStyleImage())


        self.obliqueSlicerObject = slices.Slicer(self.modelWindow, self.modelRenderer, self.axeObject)
        actorObliqueBrain = self.obliqueSlicerObject.getBrainSlices("coronal", brain_path=brain_path)
        self.obliqueRenderer.AddActor(actorObliqueBrain)
        self.actorObliqueTumor = self.obliqueSlicerObject.getTumorSlices("coronal", segmentation_path=segmentation_path)
        #self.obliqueRenderer.AddActor(self.actorObliqueTumor)

        self.obliqueInteractor.Initialize()
        self.obliqueSlicerObject.setInteraction(self.obliqueInteractor, self.obliqueWindow, self.ui.obliqueLabel)

        # actions
        self.ui.menuSlicesShowBoundariesCheckbox.clicked.connect(self.showTumorMask)
        self.ui.menuSlicesShowValueGTSlider.valueChanged.connect(self.setMinSlicePixelValue)

        self.ui.menuModelBrainCheckbox.clicked.connect(self.showVolume)
        self.ui.menuModelHealthBrainCheckBox.clicked.connect(self.showBrainVolume)
        self.ui.menuModelTumorCheckbox.clicked.connect(self.showTumorVolume)
        self.ui.menuModelHealthBrainOpacitySlider.valueChanged.connect(self.setHealthBrainOpacity)
        self.ui.menuModelTumorOpacitySlider.valueChanged.connect(self.setTumorOpacity)
        self.ui.menuModelAxesCheckbox.clicked.connect(self.showAxes)
        self.ui.menuModelTumorInFrontrCheckbox.clicked.connect(self.tumorToFront)
        self.ui.menuModelTumorHighlightCheckbox.clicked.connect(self.highlightTumor)
        self.ui.menuModelTumorRecolorCheckbox.clicked.connect(self.recolorTumor)

    def showTumorVolume(self):
        if self.ui.menuModelTumorCheckbox.checkState() == 2:
            self.modelRenderer.AddActor(self.tumorModel)
            if self.ui.menuModelHealthBrainCheckBox.checkState() == 2:
                self.ui.menuModelBrainCheckbox.setChecked(True)
                if self.ui.menuModelTumorInFrontrCheckbox.checkState() == 0:
                    self.modelRenderer.RemoveActor(self.healthBrainModel)
                    self.modelRenderer.AddActor(self.healthBrainModel)

            self.modelWindow.Render()

        elif self.ui.menuModelTumorCheckbox.checkState() == 0:
            self.modelRenderer.RemoveActor(self.tumorModel)
            self.modelWindow.Render()
            self.ui.menuModelBrainCheckbox.setChecked(False)

        else:
            print("menuModelTumorCheckbox: " + self.ui.menuModelTumorCheckbox.checkState())


    def showBrainVolume(self):
        if self.ui.menuModelHealthBrainCheckBox.checkState() == 2:
            self.modelRenderer.AddActor(self.healthBrainModel)
            if self.ui.menuModelTumorCheckbox.checkState() == 2:
                self.ui.menuModelBrainCheckbox.setChecked(True)
                if self.ui.menuModelTumorInFrontrCheckbox.checkState() == 2:
                    self.modelRenderer.RemoveActor(self.tumorModel)
                    self.modelRenderer.AddActor(self.tumorModel)
            self.modelWindow.Render()

        elif self.ui.menuModelHealthBrainCheckBox.checkState() == 0:
            self.modelRenderer.RemoveActor(self.healthBrainModel)
            self.modelWindow.Render()
            self.ui.menuModelBrainCheckbox.setChecked(False)

        else:
            print("menuModelHealthBrainCheckBox: " + self.ui.menuModelHealthBrainCheckBox.checkState())


    def showVolume(self):
        if self.ui.menuModelBrainCheckbox.checkState() == 2:
            self.ui.menuModelHealthBrainCheckBox.setChecked(True)
            self.ui.menuModelTumorCheckbox.setChecked(True)
        elif self.ui.menuModelBrainCheckbox.checkState() == 0:
            self.ui.menuModelHealthBrainCheckBox.setChecked(False)
            self.ui.menuModelTumorCheckbox.setChecked(False)
        else:
            print("menuModelBrainCheckbox: " + self.ui.menuModelBrainCheckbox.checkState())

        self.showBrainVolume()
        self.showTumorVolume()


    def setHealthBrainOpacity(self):
        value = self.ui.menuModelHealthBrainOpacitySlider.value()

        self.ui.menuModelHealthBrainOpacityLabel.setText(_translate("MainWindow", "Opacity: " + str(value) + "%", None))

        self.brainVolumeObject.setVolumeOpacity(value / 100, self.modelWindow)


    def setTumorOpacity(self):
        value = self.ui.menuModelTumorOpacitySlider.value()

        self.ui.menuModelTumorOpacityLabel.setText(_translate("MainWindow", "Opacity: " + str(value) + "%", None))

        self.tumorVolumeObject.setVolumeOpacity(value / 100, self.modelWindow)


    def tumorToFront(self):
        if self.ui.menuModelTumorInFrontrCheckbox.checkState() == 2:
            self.showBrainVolume()
        elif self.ui.menuModelTumorInFrontrCheckbox.checkState() == 0:
            self.showTumorVolume()
        else:
            print("menuModelTumorInFrontrCheckbox" + self.ui.menuModelTumorInFrontrCheckbox.checkState())


    def highlightTumor(self):
        if self.ui.menuModelTumorHighlightCheckbox.checkState() == 2:
            if self.ui.menuModelTumorRecolorCheckbox.checkState() == 2:
                self.ui.menuModelTumorRecolorCheckbox.setChecked(False)

            if self.ui.menuModelTumorInFrontrCheckbox.checkState() == 2:
                self.tumorVolumeObject.highlightVolume(self.modelWindow, 2)
            if self.ui.menuModelTumorInFrontrCheckbox.checkState() == 0:
                self.tumorVolumeObject.highlightVolume(self.modelWindow, 5)

            self.ui.menuModelHealthBrainOpacitySlider.setValue(7)
            self.setHealthBrainOpacity()
            self.ui.menuModelTumorOpacitySlider.setValue(100)
            self.setTumorOpacity()

        elif self.ui.menuModelTumorHighlightCheckbox.checkState() == 0:
            self.tumorVolumeObject.highlightVolume(self.modelWindow, 1)
        else:
            print("menuModelTumorHighlightCheckbox" + self.ui.menuModelTumorHighlightCheckbox.checkState())

    def recolorTumor(self):
        if self.ui.menuModelTumorRecolorCheckbox.checkState() == 2:
            if self.ui.menuModelTumorHighlightCheckbox.checkState() == 2:
                self.ui.menuModelTumorHighlightCheckbox.setChecked(False)

            self.tumorVolumeObject.recolorVolume(self.modelWindow)

        elif self.ui.menuModelTumorRecolorCheckbox.checkState() == 0:
            self.tumorVolumeObject.highlightVolume(self.modelWindow, 1)
        else:
            print("menuModelTumorRecolorCheckbox" + self.ui.menuModelTumorRecolorCheckbox.checkState())


    def showAxes(self):
        if self.ui.menuModelAxesCheckbox.checkState() == 2:
            self.axeObject.paintAxeX([0, self.axeObject.getCenterY(), self.axeObject.getCenterZ()],
                                [250, self.axeObject.getCenterY(), self.axeObject.getCenterZ()], self.modelRenderer)
            self.axeObject.paintAxeY([self.axeObject.getCenterX(), 0, self.axeObject.getCenterZ()],
                                [self.axeObject.getCenterX(), 250, self.axeObject.getCenterZ()], self.modelRenderer)
            self.axeObject.paintAxeZ([self.axeObject.getCenterX(), self.axeObject.getCenterY(), 0],
                                [self.axeObject.getCenterX(), self.axeObject.getCenterY(), 250], self.modelRenderer)
            self.modelWindow.Render()
            self.axeObject.setDisplay(True)

        elif self.ui.menuModelAxesCheckbox.checkState() == 0:
            self.modelRenderer.RemoveActor(self.axeObject.getAxeX())
            self.modelRenderer.RemoveActor(self.axeObject.getAxeY())
            self.modelRenderer.RemoveActor(self.axeObject.getAxeZ())
            self.modelWindow.Render()
            self.axeObject.setDisplay(False)
        else:
            print("menuModelAxesCheckbox" + self.ui.menuModelAxesCheckbox.checkState())


    def showTumorMask(self):
        if self.ui.menuSlicesShowBoundariesCheckbox.checkState() == 2:
            self.axialRenderer.AddActor(self.actorAxialTumor)
            self.axialWindow.Render()

            self.sagittalRenderer.AddActor(self.actorSagittalTumor)
            self.sagittalWindow.Render()

            self.obliqueRenderer.AddActor(self.actorObliqueTumor)
            self.obliqueWindow.Render()

        elif self.ui.menuSlicesShowBoundariesCheckbox.checkState() == 0:
            self.axialRenderer.RemoveActor(self.actorAxialTumor)
            self.axialWindow.Render()

            self.sagittalRenderer.RemoveActor(self.actorSagittalTumor)
            self.sagittalWindow.Render()

            self.obliqueRenderer.RemoveActor(self.actorObliqueTumor)
            self.obliqueWindow.Render()

        else:
            print("menuSlicesShowBoundariesCheckbox" + self.ui.menuSlicesShowBoundariesCheckbox.checkState())


    def setMinSlicePixelValue(self):
        value = self.ui.menuSlicesShowValueGTSlider.value()

        self.ui.menuSlicesShowValuesGTLabel.setText(_translate("MainWindow", "Show values equal or greater than: " + str(value), None))

        self.axialSlicerObject.setMinValue(value, self.axialWindow)
        self.sagittalSlicerObject.setMinValue(value, self.sagittalWindow)
        self.obliqueSlicerObject.setMinValue(value, self.obliqueWindow)


try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

def save_volume(data, path=None, name="new"):
    sitk_result = sitk.GetImageFromArray(data)
    if path is None:
        path = ""
    print(path)
    sitk.WriteImage(sitk_result, path +"/"+ name + ".mha")

    return 1

if __name__ == "__main__":

    # show results of steps ?
    show_results = False
    # choose index of examination to visualization
    index = 1 # 0 default
    # choose volume for visualization
    visualize_original = True       # default True if visualize_processed and visualize_warped are False
    visualize_processed = False
    visualize_warped = False
    # choose modality for visualization
    visualize_flair = True          # default True if visualize_t1, visualize_t1c and visualize_t2 are False
    visualize_t1 = False
    visualize_t1c = False
    visualize_t2 = False

    # load all data from one patient
    patient_examinations_paths = [f.path for f in os.scandir(path_to_patient) if f.is_dir()]
    reader = Reader()

    if index > len(patient_examinations_paths):
        index = 0

    for path in patient_examinations_paths:
        patient_examinations.append(reader.read(patient_examination_path=path))

    # sort examinations
    patient_examinations.sort(key=lambda x: x.name, reverse=False)

    if visualize_processed or visualize_warped:
        processed_patient_examinations = copy.deepcopy(patient_examinations)

        # PREPROCESSING

        if visualize_t1:
            # t1
            preprocessing_t1 = Preprocessing(template=processed_patient_examinations[0].mri_t1.data, ignore_zero_values=True, remove_percentile=True, help_print=show_results)
            processed_patient_examinations[0].mri_t1.data = preprocessing_t1.get_processed_teplate()
            save_volume(data=processed_patient_examinations[0].mri_t1.data, name='preprocessed_t1', path=processed_patient_examinations[0].path)
        elif visualize_t1c:
            # t1c
            preprocessing_t1c = Preprocessing(template=processed_patient_examinations[0].mri_t1c.data, ignore_zero_values=True, remove_percentile=True, help_print=show_results)
            processed_patient_examinations[0].mri_t1c.data = preprocessing_t1c.get_processed_teplate()
            save_volume(data=processed_patient_examinations[0].mri_t1c.data, name='preprocessed_t1c', path=processed_patient_examinations[0].path)
        elif visualize_t2:
            # t2
            preprocessing_t2 = Preprocessing(template=processed_patient_examinations[0].mri_t2.data, ignore_zero_values=True, remove_percentile=True, help_print=show_results)
            processed_patient_examinations[0].mri_t2.data = preprocessing_t2.get_processed_teplate()
            save_volume(data=processed_patient_examinations[0].mri_t2.data, name='preprocessed_t2', path=processed_patient_examinations[0].path)
        else: #visualize_flair - default
            # flair
            preprocessing_flair = Preprocessing(template=processed_patient_examinations[0].mri_flair.data, ignore_zero_values=True, remove_percentile=True, help_print=show_results)
            processed_patient_examinations[0].mri_flair.data = preprocessing_flair.get_processed_teplate()
            save_volume(data=processed_patient_examinations[0].mri_flair.data, name='preprocessed_flair', path=processed_patient_examinations[0].path)

        for idx in range(1, len(processed_patient_examinations)):
            print(" - next:", processed_patient_examinations[idx].name)

            if visualize_t1:
                # t1
                processed_patient_examinations[idx].mri_t1.data = preprocessing_t1.process_data(processed_patient_examinations[idx].mri_t1.data)
                save_volume(data=processed_patient_examinations[idx].mri_t1.data, name='preprocessed_t1', path=processed_patient_examinations[idx].path)
            elif visualize_t1c:
                # t1c
                processed_patient_examinations[idx].mri_t1c.data = preprocessing_t1c.process_data(processed_patient_examinations[idx].mri_t1c.data)
                save_volume(data=processed_patient_examinations[idx].mri_t1c.data, name='preprocessed_t1c', path=processed_patient_examinations[idx].path)
            elif visualize_t2:
                # t2
                processed_patient_examinations[idx].mri_t2.data = preprocessing_t2.process_data(processed_patient_examinations[idx].mri_t2.data)
                save_volume(data=processed_patient_examinations[idx].mri_t2.data, name='preprocessed_t2', path=processed_patient_examinations[idx].path)
            else:  # visualize_flair - default
                # flair
                processed_patient_examinations[idx].mri_flair.data = preprocessing_flair.process_data(processed_patient_examinations[idx].mri_flair.data)
                save_volume(data=processed_patient_examinations[idx].mri_flair.data, name='preprocessed_flair', path=processed_patient_examinations[idx].path)

        if visualize_warped:
            nrr = NonRigidRegistration(registration_type='Farneback', show_results=show_results)
            if visualize_t1:
                # t1
                registered_examinations_one_modality_only = nrr.process_volumes(processed_patient_examinations, modality='t1')
            elif visualize_t1c:
                # t1c
                registered_examinations_one_modality_only = nrr.process_volumes(processed_patient_examinations, modality='t1c')
            elif visualize_t2:
                # t2
                registered_examinations_one_modality_only = nrr.process_volumes(processed_patient_examinations, modality='t2')
            else:  # visualize_flair - default
                # flair
                registered_examinations_one_modality_only = nrr.process_volumes(processed_patient_examinations, modality='flair')

            for tmp in registered_examinations_one_modality_only[:-1]:
                save_volume(data=tmp.warped_data.data, name='volume_warped', path=tmp.path)

    volume_segmentation_path = None
    volume_brain_path = None

    # find all possible mha files
    resulting_files = []
    for (path, dirs, files) in os.walk(patient_examinations[index].path):
        for file in files:
            if file.endswith(".mha"):
                resulting_files.append(os.path.join(path, file))

    ## choose volumes depending on params
    if visualize_warped:
        # # to visualize registered data - you can choose different index of patient examination
        volume_brain = registered_examinations_one_modality_only[index].warped_data
        for file in resulting_files:
            if file.find('volume_warped') is not -1:
                volume_brain_path = file
    elif visualize_processed:
        if visualize_t1:
            # t1
            volume_brain = processed_patient_examinations[index].mri_t1
            for file in resulting_files:
                if file.find('preprocessed_t1') is not -1:
                    volume_brain_path = file
        elif visualize_t1c:
            # t1c
            volume_brain = processed_patient_examinations[index].mri_t1c
            for file in resulting_files:
                if file.find('preprocessed_t1c') is not -1:
                    volume_brain_path = file
        elif visualize_t2:
            # t2
            volume_brain = processed_patient_examinations[index].mri_t2
            for file in resulting_files:
                if file.find('preprocessed_t2') is not -1:
                    volume_brain_path = file
        else: # visualize_flair - defult
            # flair
            volume_brain = processed_patient_examinations[index].mri_flair
            for file in resulting_files:
                if file.find('preprocessed_flair') is not -1:
                    volume_brain_path = file
    else: # visualiza_original - default
        # # to visualize original data - you can choose different index of patient examination

        if visualize_t1:
            # t1
            volume_brain = patient_examinations[index].mri_t1
            for file in resulting_files:
                if file.find('.MR_T1.') is not -1:
                    volume_brain_path = file
        elif visualize_t1c:
            # t1c
            volume_brain = patient_examinations[index].mri_t1c
            for file in resulting_files:
                if file.find('.MR_T1c.') is not -1:
                    volume_brain_path = file
        elif visualize_t2:
            # t2
            volume_brain = patient_examinations[index].mri_t2
            for file in resulting_files:
                if file.find('MR_T2.') is not -1:
                    volume_brain_path = file
        else: # visualize_flair - defult
            # flair
            volume_brain = patient_examinations[index].mri_flair
            for file in resulting_files:
                if file.find('.MR_Flair.') is not -1:
                    volume_brain_path = file

    # segmentation
    volume_segmentation = patient_examinations[index].segmentation
    for file in resulting_files:
        if file.find('more.') is not -1:
            volume_segmentation_path = file

    # visualization initialization
    if volume_segmentation_path is not None and volume_brain_path is not None:
        print("Brain path:", volume_brain_path)
        print("Segmentation path:", volume_segmentation_path)
        app = QtGui.QApplication(sys.argv)
        myapp = MyForm(vis_volume=volume_brain, vis_segmentation=volume_segmentation, brain_path=volume_brain_path, segmentation_path=volume_segmentation_path)
        myapp.show()
        myapp.modelInteractor.Initialize()
        sys.exit(app.exec_())

    else:
        if volume_segmentation_path is None:
            print("Mha segmentation file was not found")
        if volume_brain_path is None:
            print("Mha volume file was not found")