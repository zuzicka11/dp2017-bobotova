# README #


DP 2018 - Bobotova - Monitoring brain tumor changes

Author/Authors: Bc. Zuzana Bobotova

Diploma thesis: Processing of three-dimensional medical data using computer vision methods

Supervisor: doc. Ing. Vanda Benesova, PhD.

Keywords: MRI, tumor, brain, keypoint detection, SIFT, SURF, FAST, ORB, transformation, Farneback dense optical flow, triangulation, tumor changes, visualization, simple tool

Year and month of termination: May 2018

Thesis Description:

The work is focused on the methods suitable for monitoring development of the brain tumor disease. At the approach, we work with magnetic resonance data. It is a medical test that is the most commonly used for diagnosing the brain cancer and also during the treatment - mainly for radiation planning and tumor changes observation. In order to track tumor changes over time, it is necessary to segment tumors from the volumes and also register data from different examinations performed in regular time periods. We have analyzed some existing methods for tumor brain segmentation and tested one of them in our research. The best methods for tumor segmentation are based on the convolutional neural networks. Subsequently we have analyzed state of the registration problematic and proposed a method for rigid and also non-rigid registration. Rigid registration can align data from different examinations. The process consists of: finding key points, finding matches between key points, filtering matches and rigid transformation. On the other side, non-rigid registration allows track tumor and brain changes between the examinations captured during the treatment process. We proposed a simple method based on the optical flow. The approach finds corresponding points in two different examinations. In order, the points are used to visualize the changes using image morphing algorithm. Last part of the work is a simple visualization tool that offers different views on the data.

Video about the work: youtu.be/_bJrEDOq1uo


## RUN ##

To run the project, first download the sources. Use terminal:


```
#!shell

$ git clone git@bitbucket.org:zuzicka11/dp2017-bobotova.git
```

## Requirements ##

* Ubuntu (project was developed on Ubuntu 16.04)
* Python 3.5.2
* OpenCV 3.2.0
* TensorFlow 1.0.0

To prepare environment for the project follow [installation guide on the wiki](https://bitbucket.org/zuzicka11/dp2017-bobotova/wiki/Instalation%20guide).